/* eslint-disable func-names, prefer-arrow-callback */
import { Meteor } from 'meteor/meteor';

import { Equipments } from '../equipments';

// All equipments
Meteor.publish('equipments', function () {
  const workshops = ['public'];
  let userLogged;

  if (this.userId) {
    userLogged = Meteor.users.findOne(this.userId);
    workshops.push(userLogged.profile.workshops);

    // Admin
    if (userLogged.profile.admin === true) {
      return Equipments.find({}, { fields: Equipments.publicFields });
    }
  }

  return Equipments.find({
    $and: [
      { active: true },
      { workshops: { $in: workshops } },
    ],
  }, {
    fields: Equipments.publicFields,
  });
});
