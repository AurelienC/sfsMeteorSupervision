/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */
import { Meteor } from 'meteor/meteor';
import { chai } from 'meteor/practicalmeteor:chai';
import { Random } from 'meteor/random';
import faker from 'faker';

import { Equipments } from '../equipments';
import './publications';
import '../methods';

// Unit tests for Equipmens collection
describe('Equipments', function () {
  let equipmentId;
  let equipment;

  describe('methods', function () {
    // Before all, clear the collection
    before(function () {
      Equipments.remove({});

      // Create a moke equipment
      equipment = {
        name: faker.lorem.word(),
        number: faker.random.number(),
        part: faker.hacker.noun(),
        active: true,
        cron: faker.random.number(5, 10),
        workshops: [faker.commerce.department()],
        connection: {
          ip: faker.internet.ip(),
        },
      };
    });

    // Get a moke userId
    const invocation = { userId: Random.id() };


    // UNIT TEST
    // for add a new document into database
    it('insert', function () {
      const insertEquipment = Meteor.server.method_handlers['equipments.insert'];

      equipmentId = insertEquipment.apply(invocation, [equipment]);

      _.extend(equipment, { _id: equipmentId });

      // Verify if only 1 document into collection
      chai.assert.equal(Equipments.find({}).count(), 1);
      // Verify if documents are equals
      chai.assert.deepEqual(Equipments.findOne(equipmentId), equipment);
    });


    // UNIT TEST
    // for update an existing document into database
    it('update', function () {
      const updateEquipment = Meteor.server.method_handlers['equipments.update'];

      // Modify local document
      equipment.name = `${equipment.name} test`;
      equipment.number = faker.random.number();

      updateEquipment.apply(invocation, [equipmentId, _.omit(equipment, '_id')]);

      // Verify if only 1 document into collection
      chai.assert.equal(Equipments.find({}).count(), 1);
      // Verify if documents are equals
      chai.assert.deepEqual(Equipments.findOne(equipmentId), equipment);
    });


    // UNIT TEST
    // for modify the state of an existing document into database
    it('set (in)active', function () {
      const setActiveEquipment = Meteor.server.method_handlers['equipments.setActive'];

      // Verify to inactive equipment
      setActiveEquipment.apply(invocation, [equipmentId, false]);
      chai.assert.isFalse(Equipments.findOne(equipmentId).active);

      // Verify to active equipment
      setActiveEquipment.apply(invocation, [equipmentId, true]);
      chai.assert.isTrue(Equipments.findOne(equipmentId).active);
    });

    // UNIT TEST
    // for modify an existing document into database
    it('change cron', function () {
      const changeCronEquipment = Meteor.server.method_handlers['equipments.changeCron'];
      const newCron = equipment.cron + 1;

      // Verify if cron is correctly updated
      changeCronEquipment.apply(invocation, [equipmentId, newCron]);
      chai.assert.equal(Equipments.findOne(equipmentId).cron, newCron);

      // Verify is it impossible to save a negative cron
      chai.assert.throws(() => {
        changeCronEquipment.apply(invocation, [equipmentId, faker.random.number(-1, -1000)]);
      }, /Interval/);
    });


    // UNIT TEST
    // for remove an existing document into database
    it('remove', function () {
      const deleteEquipment = Meteor.server.method_handlers['equipments.remove'];

      deleteEquipment.apply(invocation, [equipmentId]);

      // Verify is the equipment is removed
      chai.assert.equal(Equipments.find({}).count(), 0);
    });
  });
});
