import { Mongo } from 'meteor/mongo';
import { Factory } from 'meteor/dburles:factory';
import faker from 'faker';

import { EquipmentSchema } from './schema';

faker.locale = 'fr';

export const Equipments = new Mongo.Collection('equipments');
export default 'Equipments';


// Associate the schema for checking values
Equipments.schema = EquipmentSchema;
Equipments.attachSchema(Equipments.schema, { transform: true });

// Public fields (published to the client)
Equipments.publicFields = {
  name: 1,
  number: 1,
  part: 1,
  rate: 1,
  active: 1,
  cron: 1,
  workshops: 1,
  connection: 1,
  settings: 1,
  informations: 1,
  state: 1,
};

// Deny all client-side operations
Equipments.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

// Factory for tests
Factory.define('equipment', Equipments, {
  name: faker.lorem.word(),
  number: faker.random.number(),
  part: faker.hacker.noun(),
  active: true,
  cron: faker.random.number(),
  workshops: [faker.commerce.department()],
  connection: {
    ip: faker.internet.ip(),
  },
  informations: [
    {
      id: 1,
      name: faker.random.word(),
      oeeId: faker.random.number(0, 4),
      activeOee: true,
    }, {
      id: 2,
      name: faker.random.word(),
      oeeId: faker.random.number(0, 4),
      activeOee: false,
    },
  ],
});
