import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export default 'EquipmentSchema';

const EquipmentIoSettingsSchema = new SimpleSchema({
  goodInput: {
    type: Boolean,
    label: 'Inversion entrée comptage pièces (bonnes) (I0)',
    defaultValue: false,
  },
  badInput: {
    type: Boolean,
    label: 'Inversion entrée comptage pièces mauvaises (I1)',
    defaultValue: false,
  },
  runningInput: {
    type: Boolean,
    label: 'Inversion entrée équipement en production (I3)',
    defaultValue: false,
  },
  stoppedInput: {
    type: Boolean,
    label: 'Inversion entrée équipement hors production (I4)',
    defaultValue: false,
  },
  manualInput: {
    type: Boolean,
    label: 'Inversion entrée équipement en montage/manuel (I5)',
    defaultValue: false,
  },
  faultInput: {
    type: Boolean,
    label: 'Inversion entrée équipement en défaut (I6)',
    defaultValue: false,
  },
  information1Input: {
    type: Boolean,
    label: 'Inversion entrée 1 information (I7)',
    defaultValue: false,
  },
  information2Input: {
    type: Boolean,
    label: 'Inversion entrée 2 information (I8)',
    defaultValue: false,
  },
  information3Input: {
    type: Boolean,
    label: 'Inversion entrée 3 information (I9)',
    defaultValue: false,
  },
  information4Input: {
    type: Boolean,
    label: 'Inversion entrée 4 information (I10)',
    defaultValue: false,
  },
  countdownOutput: {
    type: Boolean,
    label: 'Inversion sortie décompteur (O0)',
    defaultValue: false,
  },
});

const EquipmentTimerSettingsSchema = new SimpleSchema({
  beforeRunning: {
    type: Number,
    label: 'Durée avant marche (x1s)',
    min: 0,
    max: 255,
    defaultValue: 5,
  },
  beforeStopped: {
    type: Number,
    label: 'Durée avant arrêt (x1s)',
    min: 0,
    max: 255,
    defaultValue: 5,
  },
  beforeFault: {
    type: Number,
    label: 'Durée avant défaut (x1s)',
    min: 0,
    max: 255,
    defaultValue: 5,
  },
  beforeManual: {
    type: Number,
    label: 'Durée avant manuel/montage (x1s)',
    min: 0,
    max: 255,
    defaultValue: 5,
  },
  topCycle: {
    type: Number,
    label: 'Durée top cycle maximum entre deux pièces (x1s)',
    min: 0,
    max: 255,
    defaultValue: 5,
  },
});

const EquipmentPrioritiesSettingsSchema = new SimpleSchema({
  run: {
    type: Number,
    label: 'Priorité de l\'information équipement en production',
    min: 0,
    max: 3,
    defaultValue: 0,
  },
  stop: {
    type: Number,
    label: 'Priorité de l\'information équipement hors production',
    min: 0,
    max: 3,
    defaultValue: 2,
  },
  manual: {
    type: Number,
    label: 'Priorité de l\'information équipement en mode manuel/montage',
    min: 0,
    max: 3,
    defaultValue: 1,
  },
  fault: {
    type: Number,
    label: 'Priorité de l\'information équipement en défaut',
    min: 0,
    max: 3,
    defaultValue: 3,
  },
});

const EquipmentSettingsSchema = new SimpleSchema({
  send: {
    type: Boolean,
    label: 'Demande d\'envoi des paramètres',
    defaultValue: false,
  },
  inversion: {
    type: EquipmentIoSettingsSchema,
  },
  priorities: {
    type: EquipmentPrioritiesSettingsSchema,
  },
  runningMode: {
    type: Number,
    min: 0,
    max: 2,
    label: 'Type de mode de marche',
    defaultValue: 0,
  },
  informationType: {
    type: Number,
    min: 0,
    max: 1,
    label: 'Type d\'information binaire',
    defaultValue: 0,
  },
  timers: {
    type: EquipmentTimerSettingsSchema,
  },
  countingMode: {
    type: Number,
    min: 0,
    max: 2,
    label: 'Type de mode de comptage',
    defaultValue: 0,
  },
  goodPartFactor: {
    type: Number,
    min: 0,
    max: 255,
    label: 'Facteur pièces bonnes (I0)',
    defaultValue: 1,
  },
  badPartFactor: {
    type: Number,
    min: 0,
    max: 255,
    label: 'Facteur pièces mauvaises (I2)',
    defaultValue: 1,
  },
  displayCountdown: {
    type: Boolean,
    label: 'Afficher le décompteur',
    defaultValue: true,
  },
  partsRateCalculation: {
    type: Number,
    min: 0,
    max: 255,
    label: 'Nombre de pièces entre chaque calcul de la cadence',
    defaultValue: 3,
  },
  displayBadParts: {
    type: Boolean,
    label: 'Afficher le compteur de pièces mauvaises',
    defaultValue: true,
  },
});

const EquipmentConnectionSchema = new SimpleSchema({
  ip: {
    type: String,
    label: 'Adresse IP',
    regEx: SimpleSchema.RegEx.IP,
  },
  port: {
    type: Number,
    min: 0,
    label: 'Port Modbus TCP-IP',
    defaultValue: 502,
    optional: true,
  },
  id: {
    type: Number,
    min: 0,
    max: 255,
    label: 'Modbus ID',
    defaultValue: 255,
    optional: true,
  },
  offset: {
    type: Number,
    min: 0,
    label: 'Début adresse mémoire',
    defaultValue: 200,
    optional: true,
  },
});

const EquipmentStateSchema = new SimpleSchema({
  name: {
    type: String,
    label: 'Nom de l\'événement',
  },
  eventTypeId: {
    type: String,
    label: 'Numéro de l\'événement',
  },
  information: {
    type: String,
    label: 'Information',
    optional: true,
  },
  secondary: {
    type: Boolean,
    label: 'Equipement en mode secondaire',
  },
  start: {
    type: Date,
    label: 'Date et heure du début de l\'événement',
  },
  rate: {
    type: Number,
    min: 0,
    label: 'Cadence instantanée (pièces/h)',
  },
  good: {
    type: Number,
    min: 0,
    label: 'Quantité de pièces bonnes',
  },
  bad: {
    type: Number,
    min: 0,
    label: 'Quantité de pièces mauvaises',
  },
  po: {
    type: Number,
    min: 0,
    label: 'Ordre de fabrication',
  },
  article: {
    type: Number,
    min: 0,
    label: 'Article',
  },
  countdown: {
    type: Number,
    label: 'Décompteur (restant)',
  },
  icon: {
    type: String,
    label: 'Icône',
    min: 0,
  },
  color: {
    type: String,
    label: 'Couleur',
    min: 0,
  },
  lastUpdated: {
    type: Date,
    label: 'Date et heure de la dernière mise à jour',
  },
});

const EquipmentInformationSchema = new SimpleSchema({
  id: {
    type: Number,
    min: 0,
    label: 'Numéro du défaut',
  },
  name: {
    type: String,
    label: 'Nom de défaut',
  },
  activeOee: {
    type: Boolean,
    label: 'Type TRS différent',
    defaultValue: false,
  },
  oeeId: {
    type: Number,
    min: 0,
    label: 'Type selon le TRS',
    optional: true,
  },
});


export const EquipmentSchema = new SimpleSchema({
  _id: {
    type: String,
    label: 'Identifiant dans la base de donnée',
    regEx: SimpleSchema.RegEx.Id,
    optional: true,
  },
  name: {
    type: String,
    label: 'Nom',
    max: 200,
  },
  number: {
    type: Number,
    min: 0,
    label: 'Numéro',
    optional: true,
  },
  part: {
    type: String,
    defaultValue: 'pièce',
    label: 'Nom des pièces produites',
  },
  rate: {
    type: Number,
    min: 0,
    label: 'Cadence théorique (pièces/h)',
    defaultValue: 0,
  },
  active: {
    type: Boolean,
    label: 'Actif',
    defaultValue: false,
  },
  cron: {
    type: Number,
    min: 0,
    label: 'Interval d\'interrogation (s)',
    defaultValue: 5,
  },
  workshops: {
    type: [String],
    label: 'Ateliers',
    optional: true,
  },
  connection: {
    type: EquipmentConnectionSchema,
    optional: true,
  },
  settings: {
    type: EquipmentSettingsSchema,
    optional: false,
  },
  informations: {
    optional: true,
    type: [EquipmentInformationSchema],
  },
  state: {
    optional: true,
    type: EquipmentStateSchema,
  },
});
