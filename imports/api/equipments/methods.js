/* eslint-disable meteor/audit-argument-checks */
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

import { Equipments } from './equipments';
import { EventsType } from '../eventsType/eventsType';
import { Events } from '../events/events';

// Client methods
Meteor.methods({
  'equipments.insert': (equipment) => {
    if (!Meteor.isServer && !Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    return Equipments.insert(equipment, { validate: true });
  },

  'equipments.update': (equipmentId, equipment) => {
    if (!Meteor.isServer && !Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    return Equipments.update(equipmentId, { $set: _.omit(equipment, '_id') });
  },

  'equipments.setActive': (equipmentId, status) => {
    check({ active: status }, Equipments.schema.pick('active'));

    if (!Meteor.isServer && !Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    Equipments.update(equipmentId, {
      $set: {
        active: status,
      },
    });
  },

  'equipments.changeCron': (equipmentId, cronUpdated) => {
    check({ _id: equipmentId }, Equipments.schema.pick('_id'));
    check({ cron: cronUpdated }, Equipments.schema.pick('cron'));

    if (!Meteor.isServer && !Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    return Equipments.update(equipmentId, {
      $set: {
        cron: cronUpdated,
      },
    });
  },

  'equipments.remove': (equipmentId) => {
    check({ _id: equipmentId }, Equipments.schema.pick('_id'));

    if (!Meteor.isServer && !Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    Events.remove({ equipmentId });
    Equipments.remove(equipmentId);
  },

  'equipments.removeEvents': (equipmentId) => {
    check({ _id: equipmentId }, Equipments.schema.pick('_id'));

    if (!Meteor.isServer && !Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    Events.remove({ equipmentId });
  },

  'equipments.addInformation': (equipmentId, information) => {
    if (!Meteor.isServer && !Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    return Equipments.update(equipmentId, {
      $push: {
        informations: information,
      },
    });
  },

  'equipments.removeInformation': (equipmentId, informationId) => {
    if (!Meteor.isServer && !Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    return Equipments.update(equipmentId, {
      $pull: {
        informations: {
          id: informationId,
        },
      },
    });
  },
  'equipments.sendParameters': equipmentId => Equipments.update(equipmentId, {
    $set: {
      'settings.send': true,
    },
  }),
  'equipments.cancelParameters': equipmentId => Equipments.update(equipmentId, {
    $set: {
      'settings.send': false,
    },
  }),
});


// Server method
if (Meteor.isServer) {
  // Update state of equipment
  Equipments.updateState = ({ equipmentId, eventTypeId, informationId, state }) => {
    check({ state }, Equipments.schema.pick([
      'state.secondary',
      'state.start',
      'state.rate',
      'state.good',
      'state.bad',
      'state.po',
      'state.article',
      'state.countdown',
    ]));
    check({ eventTypeId, informationId }, new SimpleSchema({
      eventTypeId: {
        type: Number,
        min: 0,
      },
      informationId: {
        type: Number,
        min: 0,
      },
    }));

    const eventsTypeFinded = EventsType.findOne(eventTypeId.toString(), {
      fields: {
        name: 1,
        icon: 1,
        color: 1,
      },
    });

    const informationFinded = Equipments.findOne({
      _id: equipmentId,
      'informations.id': informationId,
    }, {
      fields: {
        'informations.$': 1,
      },
    });

    _.extend(state, _.omit(eventsTypeFinded, '_id'));
    _.extend(state, {
      eventTypeId,
      lastUpdated: new Date(),
     });

    if (_.isUndefined(informationFinded)) {
      _.extend(state, { information: '' });
    } else {
      _.extend(state, { information: informationFinded.informations[0].name });
    }

    return Equipments.update(equipmentId, {
      $set: {
        state,
      },
    });
  };

  // Update state of equipment with 'not connected'
  Equipments.notConnectedState = ({ equipmentId }) => {
    const currentState = Equipments.findOne(equipmentId, {
      fields: {
        state: 1,
      },
    });

    if (_.isUndefined(currentState)) return null;

    const eventTypeNotConnected = EventsType.findOne({ _id: '0' });

    if ((currentState.name || '') !== eventTypeNotConnected.name) {
      const newState = _.extend(eventTypeNotConnected, {
        secondary: false,
        eventTypeId: 0,
        start: new Date(),
        rate: 0,
        good: 0,
        bad: 0,
        po: 0,
        article: 0,
        countdown: 0,
        lastUpdated: new Date(),
      });

      return Equipments.update(equipmentId, {
        $set: {
          state: newState,
        },
      });
    }

    return currentState;
  };
}
