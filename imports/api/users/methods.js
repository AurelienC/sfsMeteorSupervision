/* eslint-disable meteor/audit-argument-checks */
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { check } from 'meteor/check';


Meteor.users.deny({
  update() { return true; },
});

Meteor.methods({
  'users.setPassword': (userId, password) => {
    check(userId, String);

    if (!Meteor.isServer && !Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    return Accounts.setPassword(userId, password);
  },
  'users.update': (userId, user) => Meteor.users.update({ _id: userId }, { $set: user }),

  'users.create': user => Accounts.createUser(user),

  'users.remove': userId => Meteor.users.remove({ _id: userId }),

  'users.changeLocale': (userId, locale) => {
    check(userId, String);
    check(locale, String);
    return Meteor.users.update({ _id: userId }, { $set: { 'profile.locale': locale } });
  },
});
