import { Meteor } from 'meteor/meteor';

Meteor.publish('usersData', function () { // eslint-disable-line
  const u = Meteor.users.findOne(this.userId);

  if (this.userId) {
    if (u.profile.admin === true) {
      return Meteor.users.find({}, {
        fields: {
          _id: 1,
          username: 1,
          profile: 1,
        },
      });
    }
    return Meteor.users.find({ _id: this.userId }, {
      fields: {
        _id: 1,
        username: 1,
        profile: 1,
      },
    });
  }
  return null;
});
