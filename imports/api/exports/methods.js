import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import fs from 'fs';


// Get path
export const dirPath = Meteor.settings.path || '/var/log/supervision/';

// Client methods
Meteor.methods({
  'exports.get': () => {
    try {
      const files = fs.readdirSync(dirPath);
      const array = _.filter(files, elem => elem.slice(-4) === '.csv');
      return array;
    } catch (e) {
      throw new Meteor.Error(e);
    }
  },
  'exports.read': (fileName) => {
    check(fileName, String);
    try {
      const data = fs.readFileSync(dirPath + fileName);
      return data;
    } catch (e) {
      throw new Meteor.Error(e);
    }
  },
  'exports.delete': (fileName) => {
    check(fileName, String);
    fs.unlink(dirPath + fileName, (err) => {
      if (err) throw new Meteor.Error(err);
    });
  },
  'exports.readLog': () => {
    try {
      const data = fs.readFileSync('/var/log/nginx/error.log', 'utf8');
      return data;
    } catch (e) {
      throw new Meteor.Error(e);
    }
  },
});
