import { Mongo } from 'meteor/mongo';
import { Factory } from 'meteor/dburles:factory';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import faker from 'faker';

faker.locale = 'fr';

export const EventsType = new Mongo.Collection('eventsType');
export default 'EventsType';

// Schema of data
const EventsTypeSchema = new SimpleSchema({
  _id: {
    type: String,
    label: 'Identifiant dans la base de donnée',
    min: 0,
    max: 3,
  },
  oeeId: {
    type: Number,
    label: 'Numéro du type de TRS',
    min: 0,
  },
  name: {
    type: String,
    label: 'Nom du type d\'événement selon le TRS',
  },
  icon: {
    type: String,
    label: 'Nom de l\'icône associé',
  },
  color: {
    type: String,
    label: 'Nom de la couleur (defualt, primary, success, info, warning, danger)',
  },
});


// Associate the schema for checking values
EventsType.schema = EventsTypeSchema;
EventsType.attachSchema(EventsType.schema);

// Public fields (published to the client)
EventsType.publicFields = {
  _id: 1,
  oeeId: 1,
  name: 1,
  icon: 1,
  color: 1,
};

// Deny all client-side operations
EventsType.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

// Factory for tests
Factory.define('eventType', EventsType, {
  oeeId: () => faker.random.number(0, 4).toString(),
  name: () => faker.random.words(),
  icon: () => faker.random.word(),
  color: () => faker.random.word(),
});
