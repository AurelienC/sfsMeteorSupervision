/* eslint-disable meteor/audit-argument-checks */
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { EventsType } from './eventsType';


Meteor.methods({
  'eventsType.insert': (eventType) => {
    check(eventType, EventsType.schema);

    if (!Meteor.isServer && !Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    return EventsType.insert(eventType);
  },

  'eventsType.update': (eventTypeId, eventType) => {
    check({ _id: eventTypeId }, EventsType.schema.pick('_id'));
    check(eventType, EventsType.schema.omit('_id'));

    if (!Meteor.isServer && !Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    return EventsType.update(eventTypeId, { $set: eventType });
  },

  'eventsType.upsert': (eventTypeId, eventType) => {
    check({ _id: eventTypeId.toString() }, EventsType.schema.pick('_id'));
    check(eventType, EventsType.schema.omit('_id'));

    if (!Meteor.isServer && !Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    if (_.isUndefined(EventsType.findOne({ _id: eventTypeId.toString() }))) {
        return EventsType.insert(_.extend(eventType, { _id: eventTypeId.toString() }));
    }
      return EventsType.update({ _id: eventTypeId.toString() }, { $set: eventType });
  },

  'eventsType.remove': (eventsTypeId) => {
    check({ _id: eventsTypeId }, EventsType.schema.pick('_id'));

    if (!Meteor.isServer && !Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    EventsType.remove(eventsTypeId);
  },
});
