/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */
import { Meteor } from 'meteor/meteor';
import { chai } from 'meteor/practicalmeteor:chai';
import { Random } from 'meteor/random';
import faker from 'faker';

import { EventsType } from '../eventsType';
import './publications';
import '../methods';

// Unit tests for EventsType collection
describe('EventsType', function () {
  describe('methods', function () {
    // Before all, clear the collection
    before(function () {
      EventsType.remove({});
    });

    // Create a moke eventType
    let eventTypeId;
    const eventType = {
      _id: faker.random.number(0, 127).toString(),
      oeeId: faker.random.number(0, 3),
      name: faker.random.word(),
      icon: faker.random.word(),
      color: faker.random.word(),
    };

    // Get a moke userId
    const invocation = { userId: Random.id() };

    // UNIT TEST
    // for add a new document into database
    it('insert', function () {
      const insertEventType = Meteor.server.method_handlers['eventsType.insert'];

      eventTypeId = insertEventType.apply(invocation, [eventType]);

      // Verify if only 1 document into collection
      chai.assert.equal(EventsType.find({}).count(), 1);
      // Verify if documents are equals
      chai.assert.deepEqual(EventsType.findOne(eventTypeId), eventType);

      // Modify the local _id of document
      const saveId = eventType._id;
      eventType._id = '1024';

      // Verify if it's not possible to add an event with an _id > 999
      chai.assert.throws(() => {
        insertEventType.apply(invocation, [eventType]);
      }, '');

      // Restore the right _id
      eventType._id = saveId;
    });

    // UNIT TEST
    // for update an existing document into database
    it('update', function () {
      const updateEventType = Meteor.server.method_handlers['eventsType.update'];

      // Modify local document
      eventType.name = `${eventType.name} test`;
      eventType.icon = `${eventType.icon} test`;
      eventType.color = `${eventType.color} test`;

      updateEventType.apply(invocation, [eventTypeId, _.omit(eventType, '_id')]);

      // Verify if only 1 document into collection
      chai.assert.equal(EventsType.find({}).count(), 1);
      // Verify if documents are equals
      chai.assert.deepEqual(EventsType.findOne(eventTypeId), eventType);
    });

    // UNIT TEST
    // for remove an existing document into database
    it('remove', function () {
      const deleteEventType = Meteor.server.method_handlers['eventsType.remove'];

      deleteEventType.apply(invocation, [eventTypeId]);

      // Verify is the equipment is removed
      chai.assert.equal(EventsType.find({}).count(), 0);
    });
  });
});
