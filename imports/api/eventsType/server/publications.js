import { Meteor } from 'meteor/meteor';

import { EventsType } from '../eventsType';

// All eventsType
Meteor.publish('eventsType', () => EventsType.find({
}, {
  fields: EventsType.publicFields,
}));
