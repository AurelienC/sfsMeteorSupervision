import { Mongo } from 'meteor/mongo';
import { Factory } from 'meteor/dburles:factory';
import { Random } from 'meteor/random';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

import faker from 'faker';

faker.locale = 'fr';

export const Events = new Mongo.Collection('events');
export default 'Events';

// Schema of data
const EventsSchema = new SimpleSchema({
  _id: {
    type: String,
    label: 'Identifiant dans la base de donnée',
    regEx: SimpleSchema.RegEx.Id,
    optional: true,
  },
  equipmentId: {
    type: String,
    label: 'Identifiant de l\'équipement',
    regEx: SimpleSchema.RegEx.Id,
  },
  name: {
    type: String,
    label: 'Nom de l\'événement',
  },
  information: {
    type: String,
    label: 'Nom de l\'information',
    optional: true,
  },
  secondary: {
    type: Boolean,
    label: 'Equipement en mode secondaire',
    defaultValue: false,
    optional: true,
  },
  oeeId: {
    type: Number,
    label: 'Type d\'événement selon le TRS',
    min: 0,
  },
  start: {
    type: Date,
    label: 'Date et heure du début de l\'événement',
  },
  end: {
    type: Date,
    label: 'Date et heure de la fin de l\'événement',
  },
  rate: {
    type: Number,
    label: 'Cadence (pièces/h)',
    min: 0,
    defaultValue: 0,
    optional: true,
  },
  good: {
    type: Number,
    min: 0,
    label: 'Quantité de pièces bonnes',
    defaultValue: 0,
    optional: true,
  },
  bad: {
    type: Number,
    min: 0,
    label: 'Quantité de pièces mauvaises',
    defaultValue: 0,
    optional: true,
  },
  po: {
    type: Number,
    min: 0,
    label: 'Ordre de fabrication',
    defaultValue: 0,
    optional: true,
  },
  article: {
    type: Number,
    min: 0,
    label: 'Article',
    defaultValue: 0,
    optional: true,
  },
  icon: {
    type: String,
    label: 'Icône',
  },
  color: {
    type: String,
    label: 'Couleur',
  },
});


// Associate the schema for checking values
Events.schema = EventsSchema;
Events.attachSchema(Events.schema);


// Public fields (published to the client)
Events.publicFields = {
  equipmentId: 1,
  name: 1,
  information: 1,
  secondary: 1,
  oeeId: 1,
  start: 1,
  end: 1,
  rate: 1,
  good: 1,
  bad: 1,
  po: 1,
  article: 1,
  icon: 1,
  color: 1,
};

// Deny all client-side operations
Events.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

// Factory for tests
Factory.define('event', Events, {
  equipmentId: () => Random.id(),
  name: () => faker.lorem.word(),
  information: () => faker.lorem.words(),
  secondary: () => faker.random.boolean(),
  oeeId: () => faker.random.number(0, 10),
  start: () => faker.date.past(),
  end: () => faker.date.recent(),
  rate: () => faker.random.number(0, 1000),
  good: () => faker.random.number(0, 5000),
  bad: () => faker.random.number(0, 400),
  po: () => faker.random.number(10000000, 99999999),
  article: () => faker.random.number(1000000, 9999999),
  icon: () => faker.random.word(),
  color: () => faker.internet.color(),
});
