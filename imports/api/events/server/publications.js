/* eslint-disable func-names, prefer-arrow-callback */
import { Meteor } from 'meteor/meteor';

import { Equipments } from '../../equipments/equipments';
import { Events } from '../events';


// All events
Meteor.publish('events', function () {
  let workshops = ['public'];
  const equipments = [];
  let userLogged;

  if (this.userId) {
    userLogged = Meteor.users.findOne(this.userId);
    workshops = _.union(workshops, userLogged.profile.workshops);

    // Admin
    if (userLogged.profile.admin === true) {
      return Events.find({}, { fields: Events.publicFields });
    }
  }

  const equipmentsResult = Equipments.find({
    $and: [
      { active: true },
      { workshops: { $in: workshops } },
    ],
  }, {
    fields: { _id: 1 },
  });

  equipmentsResult.forEach((elem) => {
    equipments.push(elem._id);
  });

  return Events.find({
    equipmentId: { $in: equipments },
  }, {
    fields: Events.publicFields,
  });
});
