/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */
import { Meteor } from 'meteor/meteor';
import { Factory } from 'meteor/dburles:factory';
import { chai } from 'meteor/practicalmeteor:chai';
import { Random } from 'meteor/random';
import faker from 'faker';

import { Events } from '../events';
import { EventsType } from '../../eventsType/eventsType';
import { Equipments } from '../../equipments/equipments';
import './publications';
import '../methods';

// Unit tests for Events collection
describe('Events', () => {
  describe('methods', () => {
    let eventId;
    let event;

    // Before all, clear the collection
    before(() => {
      Events.remove({});

      // Create a moke equipment
      event = {
        equipmentId: Random.id(),
        name: faker.random.word(),
        information: faker.random.words(),
        secondary: false,
        oeeId: faker.random.number(0, 4),
        start: faker.date.past(),
        end: faker.date.recent(),
        rate: faker.random.number(10, 10000),
        good: faker.random.number(0, 5000),
        bad: faker.random.number(0, 400),
        po: faker.random.number(10000000, 99999999),
        article: faker.random.number(1000000, 9999999),
        icon: faker.random.word(),
        color: faker.internet.color(),
      };
    });


    // Get a moke userId
    const invocation = { userId: Random.id() };

    // UNIT TEST
    // for add a new document into database
    it('insert', function () {
      const insertEvent = Meteor.server.method_handlers['events.insert'];

      eventId = insertEvent.apply(invocation, [event]);

      event._id = eventId;

      // Verify if only 1 document into collection
      chai.assert.equal(Events.find({}).count(), 1);
      // Verify if documents are equals
      chai.assert.deepEqual(Events.findOne(eventId), event);
    });


    // UNIT TEST
    // for add a new document into database, like modbus task doing
    it('insert like Modbus event', function () {
      // Retreive initial event with less fields
      const modbusEvent = _.omit(event, ['_id', 'name', 'information', 'oeeId', 'color', 'icon']);


      // Create and add to database an equipent then an eventType
      const eventType = 1;
      const informationId = 2;

      const equipment = Factory.create('equipment');
      Factory.create('eventType', { _id: eventType.toString() });

      modbusEvent.equipmentId = equipment._id;

      const ModbusEventId = Events.modbusInsert({
        event: modbusEvent,
        eventTypeId: eventType,
        informationId,
      });

      modbusEvent._id = ModbusEventId;

      // Find idOee, name, icon and color for this type of event
      const eventsTypeFinded = EventsType.findOne(eventType.toString(), {
        fields: {
          oeeId: 1,
          name: 1,
          icon: 1,
          color: 1,
        },
      });

      // Find information name and idOee of information number
      const informationFinded = Equipments.findOne({
        _id: modbusEvent.equipmentId,
        'informations.id': informationId,
      }, {
        fields: {
          'informations.$': 1,
        },
      });

      const dbEvent = Events.findOne(ModbusEventId);

      // Verify if only 2 documents into collection
      chai.assert.equal(Events.find({}).count(), 2);
      // Verify if names are equals
      chai.assert.equal(dbEvent.name, eventsTypeFinded.name);
      // Verify if icons are equals
      chai.assert.equal(dbEvent.icon, eventsTypeFinded.icon);
      // Verify if colour are equals
      chai.assert.equal(dbEvent.color, eventsTypeFinded.color);
      // Verify if information are equals
      chai.assert.equal(dbEvent.information, informationFinded.informations[0].name);
      // Verify if idOee are equals
      if (informationFinded.informations[0].name) {
        chai.assert.equal(dbEvent.oeeId, informationFinded.informations[0].oeeId);
      } else {
        chai.assert.equal(dbEvent.oeeId, eventsTypeFinded.oeeId);
      }
    });


    // UNIT TEST
    // for update an existing document into database
    it('update', function () {
      const updateEvent = Meteor.server.method_handlers['events.update'];

      // Modify local document
      event.name = `${event.name} test`;
      event.icon = `${event.icon} test`;
      event.color = `${event.color} test`;

      updateEvent.apply(invocation, [eventId, _.omit(event, '_id')]);

      // Verify if only 2 documents into collection
      chai.assert.equal(Events.find({}).count(), 2);
      // verify if documents are equals
      chai.assert.deepEqual(Events.findOne(eventId), event);
    });

    // UNIT TEST
    // for remove an existing document into database
    it('remove', function () {
      const deleteEvent = Meteor.server.method_handlers['events.remove'];

      deleteEvent.apply(invocation, [eventId]);

      // Verify is the equipment is removed
      chai.assert.equal(Events.find({}).count(), 1);
    });
  });
});
