/* eslint-disable meteor/audit-argument-checks */
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { exportcsv } from 'meteor/lfergon:exportcsv';

import { Events } from './events';
import { EventsType } from '../eventsType/eventsType';
import { Equipments } from '../equipments/equipments';

// Client methods
Meteor.methods({
  'events.insert': (event) => {
    check(event, Events.schema.omit('_id'));

    if (!Meteor.isServer && !Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    return Events.insert(event);
  },

  'events.update': (eventId, event) => {
    check({ _id: eventId.toString() }, Events.schema.pick('_id'));
    check(_.omit(event, '_id'), Events.schema.omit('_id'));

    if (!Meteor.isServer && !Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    return Events.update(eventId.toString(), { $set: event });
  },

  'events.remove': (eventId) => {
    check({ _id: eventId.toString() }, Events.schema.pick('_id'));

    if (!Meteor.isServer && !Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    Events.remove(eventId.toString());
  },

  'events.toCsv': events => exportcsv.exportToCSV(events, true, ';'),

  'events.allToCsv': (equipmentId) => {
    const pipeline = [{
      $match: {
        equipmentId,
      },
    }, {
      $lookup: {
        from: 'equipments',
        localField: 'equipmentId',
        foreignField: '_id',
        as: 'equipment',
      },
    }, {
      $project: {
        information: { $ifNull: ['$information', ''] },
        start: { $dateToString: { format: '%d/%m/%Y %H:%M:%S', date: '$start' } },
        end: { $dateToString: { format: '%d/%m/%Y %H:%M:%S', date: '$end' } },
        'equipment.name': 1,
        secondary: 1,
        rate: 1,
        good: 1,
        bad: 1,
        po: 1,
        article: 1,
        oeeId: 1,
        name: 1,
        _id: 0,
      },
    }, {
      $unwind: '$equipment',
    }];

    const result = [];

    result.push([
      'equipment',
      'start',
      'end',
      'name',
      'oeeId',
      'information',
      'rate',
      'good',
      'bad',
      'po',
      'article',
      'secondary',
    ]);

    _.each(Events.aggregate(pipeline), (elem) => {
      result.push([
        elem.equipment.name,
        elem.start,
        elem.end,
        elem.name,
        elem.oeeId,
        elem.information,
        elem.rate,
        elem.good,
        elem.bad,
        elem.po,
        elem.article,
        elem.secondary,
      ]);
    });

    return exportcsv.exportToCSV(result, true, ';');
  },

  'events.getForAnalyze': (startQuery, endQuery) => {
    const pipeline = [{
      $match: {
        $and: [
          { secondary: false },
          { $or: [
            { $and: [{ start: { $gte: startQuery } }, { start: { $lte: endQuery } }] },
            { $and: [{ end: { $gte: startQuery } }, { end: { $lte: endQuery } }] },
            { $and: [
              { start: { $lte: startQuery } },
              { start: { $lte: endQuery } },
              { end: { $gte: startQuery } },
              { end: { $gte: endQuery } },
            ] },
          ],
        },
      ],
    },
  }, {
    $group: {
      _id: {
        po: '$po',
        article: '$article',
        oeeId: '$oeeId',
        information: '$information',
        equipmentId: '$equipmentId',
      },
      count: { $sum: 1 },
      good: { $sum: '$good' },
      bad: { $sum: '$bad' },
      rate: { $avg: '$rate' },
      duration: { $sum: { $subtract: ['$end', '$start'] } },
    },
  }, {
    $lookup: {
      from: 'equipments',
      localField: '_id.equipmentId',
      foreignField: '_id',
      as: 'equipment',
    },
  }, {
    $project: {
      'equipment.name': 1,
      'equipment._id': 1,
      count: 1,
      good: 1,
      bad: 1,
      rate: 1,
      duration: 1,
    },
  }, {
    $unwind: '$equipment',
  },
];

return Events.aggregate(pipeline);
},
});

// Server methods
if (Meteor.isServer) {
  Events.modbusInsert = ({ event, eventTypeId, informationId }) => {
    check(event, Events.schema.omit(['_id', 'name', 'information', 'oeeId', 'color', 'icon']));
    check({ eventTypeId, informationId }, new SimpleSchema({
      eventTypeId: {
        type: Number,
        min: 0,
      },
      informationId: {
        type: Number,
        min: 0,
      },
    }));

    // Retrieve event type
    const eventsTypeFinded = EventsType.findOne(eventTypeId.toString(), {
      fields: {
        oeeId: 1,
        name: 1,
        icon: 1,
        color: 1,
      },
    });

    // Retrieve equipment's rate
    const eqt = Equipments.findOne({ _id: event.equipmentId }, {
      fields: {
        rate: 1,
        'settings.countingMode': 1,
      },
    });

    // Retrieve equipment's informations
    const informationFinded = Equipments.findOne({
      _id: event.equipmentId,
      'informations.id': informationId,
    }, {
      fields: {
        'informations.$': 1,
        'settings.countingMode': 1,
      },
    });

    if (_.isUndefined(eventsTypeFinded)) {
      throw new Meteor.Error('not-event-type-founded');
    }

    _.extend(event, _.omit(eventsTypeFinded, '_id'));

    // Constant rate
    if (eqt.settings.countingMode === 2) {
      _.extend(event, { rate: eqt.rate });
    }

    // Information associed
    if (_.isUndefined(informationFinded)) {
      _.extend(event, { information: '' });
    } else {
      _.extend(event, { information: informationFinded.informations[0].name });
      if (informationFinded.informations[0].activeOee) {
        _.extend(event, { oeeId: informationFinded.informations[0].oeeId });
      }
    }

    return Events.insert(event);
  };
}
