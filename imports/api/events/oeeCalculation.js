import { moment } from 'meteor/momentjs:moment';

exports.oeeCalculation = ({ start, end, events, rate }) => {
  const rateData = {
    ref: rate,
    qty: 0,
    sum: 0,
    avg: 0,
  };

  const parts = {
    good: 0,
    bad: 0,
  };

  const oee = [];

  oee[1] = 0;
  oee[2] = 0;
  oee[3] = 0;
  oee[4] = 0;
  oee[10] = 0;

  // Sum of each type of OEE
  events.forEach((ev) => {
    if (!ev.secondary) {
      let evStart = moment(ev.start, moment.ISO_8601);
      let evEnd = moment(ev.end, moment.ISO_8601);
      const ea = moment(evStart).isBetween(start, end);
      const eb = moment(evEnd).isBetween(start, end);
      const ec = moment(start).isBetween(evStart, evEnd);
      const ed = moment(end).isBetween(evStart, evEnd);

      if (ea || eb || (ec && ed)) {
        // Counting parts if production time
        if (ev.oeeId === 10) {
          parts.good += ev.good;
          parts.bad += ev.bad;
          rateData.sum += (3600 * (ev.good + ev.bad)) / moment(evEnd).diff(moment(evStart), 'seconds');
          rateData.qty += 1;
        }

        // Split start date, if before
        if (moment(evStart).isBefore(start, 'second')) {
          evStart = moment(start);
        }

        // Slit end date,if after
        if (moment(evEnd).isAfter(end, 'second')) {
          evEnd = moment(end);
        }

        oee[ev.oeeId] += evEnd.diff(evStart, 'seconds');
      }
    }
  });

  rateData.avg = rateData.sum / rateData.qty;

  const totalOperationgTime = moment.duration(moment(end).diff(start, 'seconds'), 'seconds');
  const plantOperatingTime = moment.duration(totalOperationgTime).subtract(oee[4], 'seconds');
  const planedProductionTime = moment.duration(plantOperatingTime).subtract(oee[1], 'seconds');
  const operatingTime = moment.duration(planedProductionTime).subtract(oee[2], 'seconds');
  const netOperatingTime = oee[10];

  const availability = operatingTime.asSeconds() / (operatingTime.asSeconds() + oee[2]);
  const performance = (rate > 0) ? rateData.avg / rateData.ref : 1;
  const quality = (parts.bad !== 0) ? parts.good / (parts.good + parts.bad) : 1;

  const fullyProductiveTime = netOperatingTime * quality;

  const OEE = availability * performance * quality;

  const result = {
    start,
    end,
    totalOperationgTime: totalOperationgTime.asSeconds(),
    plantOperatingTime: plantOperatingTime.asSeconds(),
    planedProductionTime: planedProductionTime.asSeconds(),
    operatingTime: operatingTime.asSeconds(),
    fullyProductiveTime,
    netOperatingTime,
    availability,
    performance,
    quality,
    OEE,
  };

  return result;
};
