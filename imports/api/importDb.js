import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import faker from 'faker';

import { EventsType } from './eventsType/eventsType';

// Add admin user (bare)
const pwd = faker.internet.password();

// Print generated admin password
console.log(`admin account password : ${pwd}`);

if (!_.isUndefined(Meteor.users.findOne({ username: 'admin' }))) {
  Meteor.users.remove({ username: 'admin' });
}

const id = Accounts.createUser({
  username: 'admin',
  profile: {
    admin: true,
    locale: 'en',
    workshops: ['public'],
  },
});
Accounts.setPassword(id, pwd);


// Events type
const ids = [];
 EventsType.find({}, { fields: { _id: 1 } }).forEach((elem) => {
   ids.push(elem._id);
 });

if (_.indexOf(ids, '0') < 0) {
  EventsType.insert({
    _id: '0',
    oeeId: 4,
    name: 'Not connected',
    icon: 'fa-exchange',
    color: 'default',
  });
}

if (_.indexOf(ids, '1') < 0) {
  EventsType.insert({
    _id: '1',
    oeeId: 10,
    name: 'Running',
    icon: 'fa-play',
    color: 'success',
  });
}

if (_.indexOf(ids, '2') < 0) {
  EventsType.insert({
    _id: '2',
    oeeId: 1,
    name: 'Manual',
    icon: 'fa-wrench',
    color: 'default',
  });
}

if (_.indexOf(ids, '5') < 0) {
  EventsType.insert({
    _id: '5',
    oeeId: 2,
    name: 'Maintenance',
    icon: 'fa-exclamation-triangle',
    color: 'warning',
  });
}

if (_.indexOf(ids, '8') < 0) {
  EventsType.insert({
    _id: '8',
    oeeId: 2,
    name: 'Unknown stop',
    icon: 'fa-question',
    color: 'default',
  });
}

if (_.indexOf(ids, '9') < 0) {
  EventsType.insert({
    _id: '9',
    oeeId: 4,
    name: 'Power-off',
    icon: 'fa-toggle-off',
    color: 'info',
  });
}

if (_.indexOf(ids, '10') < 0) {
  EventsType.insert({
    _id: '10',
    oeeId: 2,
    name: 'Fault',
    icon: 'fa-exclamation',
    color: 'warning',
  });
}

if (_.indexOf(ids, '11') < 0) {
  EventsType.insert({
    _id: '11',
    oeeId: 2,
    name: 'Operator unavailability',
    icon: 'fa-user-times',
    color: 'danger',
  });
}

if (_.indexOf(ids, '12') < 0) {
  EventsType.insert({
    _id: '12',
    oeeId: 10,
    name: 'PO begin',
    icon: 'fa-bookmark-o',
    color: 'success',
  });
}

if (_.indexOf(ids, '13') < 0) {
  EventsType.insert({
    _id: '13',
    oeeId: 2,
    name: 'PO end',
    icon: 'fa-bookmark',
    color: 'primary',
  });
}

if (_.indexOf(ids, '14') < 0) {
  EventsType.insert({
    _id: '14',
    oeeId: 4,
    name: 'Power-on',
    icon: 'fa-toggle-on',
    color: 'info',
  });
}

if (_.indexOf(ids, '15') < 0) {
  EventsType.insert({
    _id: '15',
    oeeId: 1,
    name: 'Countdown stop',
    icon: 'fa-dashboard',
    color: 'primary',
  });
}

if (_.indexOf(ids, '16') < 0) {
  EventsType.insert({
    _id: '16',
    oeeId: 2,
    name: 'Wait something',
    icon: 'fa-hourglass-half',
    color: 'danger',
  });
}
