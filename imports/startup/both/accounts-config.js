/* eslint angular/module-getter: 0 */
import { Accounts } from 'meteor/accounts-base';

Accounts.config({
  loginExpirationInDays: 99,
});
