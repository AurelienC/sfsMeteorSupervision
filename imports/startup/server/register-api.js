// Equipments collection
import '../../api/equipments/methods';
import '../../api/equipments/server/publications';

// Events collection
import '../../api/events/methods';
import '../../api/events/server/publications';

// EventsType collection
import '../../api/eventsType/methods';
import '../../api/eventsType/server/publications';

// Users collection
import '../../api/users/server/publications';
import '../../api/users/methods';

// Exports
import '../../api/exports/methods';
