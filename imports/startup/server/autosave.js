import { Meteor } from 'meteor/meteor';
import { exportcsv } from 'meteor/lfergon:exportcsv';
import { moment } from 'meteor/momentjs:moment';
import Cron from 'cron';
import fs from 'fs';
import path from 'path';

import { Events } from '../../api/events/events';
import { dirPath } from '../../api/exports/methods';

// Save all events from the last month into CSV file
function task() {
  try {
    const startQuery = moment().startOf('month').subtract(1, 'month');
    const endQuery = moment().endOf('month').subtract(1, 'month');

    const pipeline = [{
      $match: {
        $and: [{ start: { $gte: startQuery.toDate() } }, { start: { $lte: endQuery.toDate() } }],
      },
    }, {
      $lookup: {
        from: 'equipments',
        localField: 'equipmentId',
        foreignField: '_id',
        as: 'equipment',
      },
    }, {
      $project: {
        information: { $ifNull: ['$information', ''] },
        start: { $dateToString: { format: '%d/%m/%Y %H:%M:%S', date: '$start' } },
        end: { $dateToString: { format: '%d/%m/%Y %H:%M:%S', date: '$end' } },
        'equipment.name': 1,
        secondary: 1,
        rate: 1,
        good: 1,
        bad: 1,
        po: 1,
        article: 1,
        oeeId: 1,
        name: 1,
        _id: 0,
      },
    }, {
      $unwind: '$equipment',
    }];

    const result = [];

    result.push([
      'equipment',
      'start',
      'end',
      'name',
      'oeeId',
      'information',
      'rate',
      'good',
      'bad',
      'po',
      'article',
      'secondary',
    ]);

    _.each(Events.aggregate(pipeline), (elem) => {
      result.push([
        elem.equipment.name,
        elem.start,
        elem.end,
        elem.name,
        elem.oeeId,
        elem.information,
        elem.rate,
        elem.good,
        elem.bad,
        elem.po,
        elem.article,
        elem.secondary,
      ]);
    });

    const csv = exportcsv.exportToCSV(result, true, ';');
    const buffer = new Buffer(csv);

    const filePath = path.join(dirPath, `${moment().get('year')}_${moment().subtract(1, 'month').get('month') + 1}_events.csv`);

    fs.access(dirPath, fs.F_OK | fs.W_OK, (err) => { // eslint-disable-line
      if (err) {
        console.error(`You must create and give access to server to folder ${dirPath}`);
      } else {
        fs.writeFileSync(filePath, buffer);
      }
    });
  } catch (e) {
    console.error(`Error with exports events folder : ${e.message}`);
  }
}

const CronJob = Cron.CronJob;
const cronTask = new CronJob('* * * 1 * *', Meteor.bindEnvironment(() => { // eslint-disable-line
  task();
}), null, true, 'Europe/Paris');
