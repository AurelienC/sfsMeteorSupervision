import { Meteor } from 'meteor/meteor';
import { moment } from 'meteor/momentjs:moment';
import modbus from 'jsmodbus';
import binary from 'binary-helper';

import { Equipments } from '../api/equipments/equipments';
import { Events } from '../api/events/events';
import Oee from '../api/events/oeeCalculation';

export default 'ModbusEquipment';

export class ModbusEquipment {
  constructor(equipmentId) {
    const equipment = Equipments.find(equipmentId, {
      fields: {
        _id: 1,
        name: 1,
        rate: 1,
        connection: 1,
        settings: 1,
      },
      options: {
        limit: 1,
      },
    });

    this.equipment = equipment.fetch()[0];

    // Observe changes
    equipment.observe({
      changed: (newEquipment) => {
        this.equipment = newEquipment;
      },
    });

    // Creation of client
    this.client = modbus.client.tcp.complete({
      host: this.equipment.connection.ip,
      port: this.equipment.connection.port,
      autoReconnect: true,
      // logEnabled: true,
      // logLevel: 'debug',
      reconnectTimeout: 10000,
      timeout: 2000,
      unitId: this.equipment.connection.id,
    });

    // Actions when connected
    // IMPORTANT ! Don't remove or move any function bellow
    this.client.on('connect', Meteor.bindEnvironment(() => {
      this.getData(Meteor.bindEnvironment(() => {
        try {
          this.extractState();
          this.extractEvent();
        } catch (e) {
          console.error(`Error when retreive information - ${e}`);
        }
        this.sendOee(Meteor.bindEnvironment(() => {
          this.sendDateTime(Meteor.bindEnvironment(() => {
            if (this.equipment.settings.send) {
              this.sendSettings(() => this.client.close());
            } else {
              this.client.close();
            }
          }));
        }));
      }));
    }));

    // On close
    this.client.on('close', () => {
      // Nothing
    });

    // On error
    this.client.on('error', Meteor.bindEnvironment(() => {
      Equipments.notConnectedState({ equipmentId: this.equipment._id });
    }));
  }


  // PERIODIC TASK
  // Called by time.js (CRON task)
  periodiqueTask() {
    this.client.connect();
  }

  // 1
  // Get data (state + event)
  getData(callback) {
    this.client.readHoldingRegisters(this.equipment.connection.offset, 30)
    .then(Meteor.bindEnvironment((resp) => {
      this.readValue = resp;
    }))
    .finally(() => {
      callback();
    });
  }


  // 2
  // Get state
  extractState() {
    const rawState = this.readValue.register.slice(0, 12);
    const idsState = this.extractIds(rawState[0]);

    const state = {
      secondary: idsState.secondary,
      start: this.extractDate(rawState[1], rawState[2], rawState[3]),
      rate: rawState[4],
      good: rawState[5],
      bad: rawState[6],
      po: binary.setMsbLsbDouble(rawState[8], rawState[7]),
      article: binary.setMsbLsbDouble(rawState[10], rawState[9]),
      countdown: binary.toSigned(rawState[11], 16),
    };

    return Equipments.updateState({
      equipmentId: this.equipment._id,
      eventTypeId: idsState.eventTypeId,
      informationId: idsState.informationId,
      state,
    });
  }


  // 3
  // Get event
  extractEvent() {
    if (this.readValue.register[12] < 0x100) {
      return false;
    }

    const rawEvent = this.readValue.register.slice(13, 30);
    const idsEvent = this.extractIds(rawEvent[0]);

    let event = {
      equipmentId: this.equipment._id,
      secondary: idsEvent.secondary,
      start: this.extractDate(rawEvent[1], rawEvent[2], rawEvent[3]),
      end: this.extractDate(rawEvent[4], rawEvent[5], rawEvent[6]),
      good: binary.setMsbLsbDouble(rawEvent[10], rawEvent[9]),
      bad: binary.setMsbLsbDouble(rawEvent[12], rawEvent[11]),
      po: binary.setMsbLsbDouble(rawEvent[14], rawEvent[13]),
      article: binary.setMsbLsbDouble(rawEvent[16], rawEvent[15]),
    };

    if (event.good > 0 || event.bad > 0) {
      let rate = Math.trunc(
        (event.good + event.bad) / (moment(event.end).diff(moment(event.start)) / 3600000),
      );
      if (rate < 0) rate = 0;
      event = _.extend(event, { rate });
    } else {
      event = _.extend(event, { rate: 0 });
    }

    return Events.modbusInsert({
      eventTypeId: idsEvent.eventTypeId,
      informationId: idsEvent.informationId,
      event,
    });
  }

  // 4
  // Send OEE and make event saved
  sendOee(callback) {
    const startQuery = moment().subtract(1, 'day')
    .toDate();
    const endQuery = moment()
    .toDate();

    const events = Events.find({
      equipmentId: this.equipment._id,
      $or: [
        { $and: [{ start: { $gte: startQuery } }, { start: { $lte: endQuery } }] },
        { $and: [{ end: { $gte: startQuery } }, { end: { $lte: endQuery } }] },
        { $and: [
          { start: { $lte: startQuery } },
          { start: { $lte: endQuery } },
          { end: { $gte: startQuery } },
          { end: { $gte: endQuery } },
        ] },
      ],
    }, {
      fields: {
        oeeId: 1,
        good: 1,
        bad: 1,
        start: 1,
        end: 1,
      },
    });

    const oee = Oee.oeeCalculation({
      start: startQuery,
      end: endQuery,
      events,
      rate: this.equipment.rate,
    });

    let oeeToSend = Math.floor(oee.OEE * 100);

    if (oeeToSend > 100) oeeToSend = 100;
    if (oeeToSend < 0) oeeToSend = 0;

    this.client.writeSingleRegister(
      this.equipment.connection.offset + 12,
      oeeToSend)
      .finally(() => {
        callback();
      });
    }


    // 5
    // Send datetime to PLC
    sendDateTime(callback) {
      const currentDt = moment();

      const buffer = [
        100 + currentDt.second(),
        binary.setBCD(currentDt.hour(), currentDt.minute()),
        binary.setBCD(currentDt.date(), currentDt.month() + 1),
        currentDt.year(),
      ];

      this.client.writeMultipleRegisters(this.equipment.connection.offset + 30, buffer)
      .finally(() => {
        callback();
      });
    }


    // 6
    // Send settings (if needed)
    sendSettings(callback) {
      let countingMode;
      switch (this.equipment.settings.countingMode) {
        case 0:
        countingMode = false;
        break;
        case 1:
        countingMode = true;
        break;
        case 2:
        countingMode = false;
        break;
        default:
        countingMode = false;
      }

      const binaryWord = [
        this.equipment.settings.inversion.goodInput,          // 1
        this.equipment.settings.inversion.badInput,           // 2
        this.equipment.settings.inversion.runningInput,       // 3
        this.equipment.settings.inversion.stoppedInput,       // 4
        this.equipment.settings.inversion.manualInput,        // 5
        this.equipment.settings.inversion.faultInput,         // 6
        this.equipment.settings.inversion.information1Input,  // 7
        this.equipment.settings.inversion.information2Input,  // 8
        this.equipment.settings.inversion.information3Input,  // 9
        this.equipment.settings.inversion.information4Input,  // 10
        this.equipment.settings.inversion.countdownOutput,    // 11
        countingMode,                                         // 12
        this.equipment.settings.displayCountdown,             // 13
        this.equipment.settings.displayBadParts,              // 14
        false,                                                // 15 spare
        true,                                                 // 16 need to update settings
      ];

      /* eslint-disable no-bitwise */
      const priorities = (this.equipment.settings.priorities.run << 6) +
      (this.equipment.settings.priorities.stop << 4) +
      (this.equipment.settings.priorities.manual << 2) +
      this.equipment.settings.priorities.fault;
      /* eslint-enable no-bitwise */

      const buffer = [
        binary.bitsToWord(binaryWord),
        binary.setMsbLsb(
          this.equipment.settings.runningMode,
          this.equipment.settings.partsRateCalculation,
        ),
        binary.setMsbLsb(
          this.equipment.settings.timers.beforeRunning,
          this.equipment.settings.timers.beforeStopped,
        ),
        binary.setMsbLsb(
          this.equipment.settings.timers.beforeManual,
          this.equipment.settings.timers.beforeFault,
        ),
        binary.setMsbLsb(
          this.equipment.settings.goodPartFactor,
          this.equipment.settings.badPartFactor,
        ),
        binary.setMsbLsb(
          this.equipment.settings.timers.topCycle,
          priorities,
        ),
      ];

      this.client.writeMultipleRegisters(this.equipment.connection.offset + 34, buffer)
      .then(Meteor.bindEnvironment(() => {
        Equipments.update(this.equipment._id, {
          $set: {
            'settings.send': false,
          },
        });
      }))
      .finally(() => {
        callback();
      });
    }


    // Helper : get ids
    extractIds(id) {
      return {
        eventTypeId: binary.getMsb(id),         // eslint-disable-line no-bitwise
        secondary: (Boolean)(id & 0x0080),      // eslint-disable-line no-bitwise
        informationId: (id & 0x007F),           // eslint-disable-line no-bitwise
      };
    }


    // Helper : get datetime
    extractDate(hhmm, ddmm, ssyy) {
      const start = moment();

      start.hour(binary.getLeftBCD(hhmm));
      start.minute(binary.getRightBCD(hhmm));
      start.date(binary.getLeftBCD(ddmm));
      start.month(binary.getRightBCD(ddmm) - 1);
      start.second(binary.getLeftBCD(ssyy));
      start.year(moment(binary.getRightBCD(ssyy), 'YY').year());

      return start.toDate();
    }
  }
