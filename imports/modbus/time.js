import Cron from 'cron';
import { Meteor } from 'meteor/meteor';
import { ModbusEquipment } from './modbus';
import { Equipments } from '../api/equipments/equipments';

const CronJob = Cron.CronJob;

const equipments = Equipments.find({ active: true }, { fields: {
  _id: 1,
  name: 1,
  cron: 1,
  active: 1,
  connection: 1,
} });

const equipmentsList = {};

// Add CRON job
function createJob(equipment) {
  const id = equipment._id;

  // Equipment already defined, stop it before new creation
  if (!_.isUndefined(equipmentsList[id])) {
    if (!_.isUndefined(equipmentsList[id].cron)) equipmentsList[id].cron.stop();
  }

    equipmentsList[id] = {};

  if (equipment.active) {
    equipmentsList[id] = _.extend(equipmentsList[id], { modbus: new ModbusEquipment(id) });

    try {
      equipmentsList[id] = _.extend(equipmentsList[id], {
        cron: new CronJob(`*/${equipment.cron} * * * * *`, Meteor.bindEnvironment(() => {
          equipmentsList[id].modbus.periodiqueTask();
        }), null, true, 'Europe/Paris'),
      });
    } catch (e) {
      console.error('Error with the CRON job creation.');
    }
  }
}

// Observe changes from Equipments (cron, active or connection)
equipments.observe({
  changed: (newEquipment, oldEquipment) => {
    if (newEquipment.cron !== oldEquipment.cron ||
      newEquipment.active !== oldEquipment.active ||
      newEquipment.connection !== oldEquipment.connection) {
        createJob(newEquipment);
      }
    },
    added: (newEquipment) => {
      createJob(newEquipment);
    },
    removed: (oldEquipment) => {
      const id = oldEquipment._id;
      if (!_.isUndefined(equipmentsList[id])) {
        equipmentsList[id].cron.stop();
      }
      _.omit(equipmentsList, id);
    },
  });

// Add all jobs
  equipments.forEach((equipment) => {
    createJob(equipment);
  });
