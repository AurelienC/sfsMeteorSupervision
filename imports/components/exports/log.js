import { Meteor } from 'meteor/meteor';
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import template from './log.html';

export const name = 'exportsLog';

class ExportsLogCtrl {
  constructor($scope, $state, $document, $translate) {
    $scope.viewModel(this);

    /* eslint-disable */
    $document[0].title = `Supervision`;
    /*eslint-enable */

    this.translate = $translate;

    Meteor.call('exports.readLog', (err, result) => {
      // Retrieve log
      this.log = result;

      // Retrieve password
      const regex = /admin account password : (\w+)/gm;
      const arr = [];
      let m;
      do {
        m = regex.exec(result);
        if (m) {
          arr.push(m[1]);
        }
      } while (m);
      this.password = (_.isUndefined(m)) ? null : arr.slice(-1)[0];
    });


    this.helpers({
      password() {
        return this.password || null;
      },
      log() {
        return this.log;
      },
      currentUser() {
        return Meteor.user();
      },
    });
  }

}

export default angular.module(name, [
  angularMeteor,
  uiRouter,
]).component(name, {
  template,
  controller: ['$scope', '$state', '$document', '$translate', ExportsLogCtrl],
});
