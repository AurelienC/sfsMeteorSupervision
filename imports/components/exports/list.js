import { Meteor } from 'meteor/meteor';
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import template from './list.html';

export const name = 'exportsList';

class ExportsListCtrl {
  constructor($scope, $state, $document, $translate) {
    $scope.viewModel(this);

    if (_.isUndefined(Meteor.user()) || !Meteor.user()) {
      $state.transitionTo('users.connection');
    }


    /* eslint-disable */
    $document[0].title = `Supervision`;
    /*eslint-enable */

    this.translate = $translate;

    Meteor.call('exports.get', (err, result) => {
      this.files = result;
    });


    this.helpers({
      files() {
        return this.files;
      },
      error() {
        if (_.isUndefined(this.error)) return '';
        return this.error;
      },
      message() {
        if (_.isUndefined(this.message)) return '';
        return this.message;
      },
      currentUser() {
        return Meteor.user();
      },
    });
  }

  // Download CSV file
  download(fileName) {
    this.error = '';
    this.message = '';
    Meteor.call('exports.read', fileName, (err, fileContent) => {
      if (err) this.error = err.message;
      if (fileContent) {
        const blob = new Blob([fileContent], { type: 'text/csv;charset=utf-8' }); // eslint-disable-line
        saveAs(blob, fileName);  // eslint-disable-line
      }
    });
  }

  // Delete file
  remove(fileName) {
    this.error = '';
    this.message = '';
    Meteor.call('exports.delete', fileName, (err) => {
      if (err) {
        this.error = err.message;
      } else {
        this.message = 'Successful operation.';
        this.files = _.without(this.files, fileName);
      }
    });
  }
}

export default angular.module(name, [
  angularMeteor,
  uiRouter,
]).component(name, {
  template,
  controller: ['$scope', '$state', '$document', '$translate', ExportsListCtrl],
});
