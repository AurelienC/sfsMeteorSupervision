import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import template from './edit.html';

export const name = 'usersEdit';

class UsersEditCtrl {
  constructor($scope, $state, $stateParams, $document, $translate) {
    $scope.viewModel(this);

    if (!Meteor.userId() || _.isUndefined(Meteor.user())) {
      $state.transitionTo('users.connection');
    }

    this.translate = $translate;
    this.state = $state;
    this.subscribe('usersData');

    // Retrieve parameters
    if ($stateParams.userId !== '' && Meteor.user().profile.admin) {
      this.userId = $stateParams.userId;
    } else if ($stateParams.userId === '' && Meteor.user().profile.admin) {
      this.userId = undefined;
    } else {
      this.userId = Meteor.userId();
    }

    // Load user data
    this.user = _.isUndefined(this.userId) ? {} : Meteor.users.findOne({ _id: this.userId });
    this.password = '';

    /* eslint-disable */
    $document[0].title = `${this.user.username || ''} - Supervision`;
    /*eslint-enable */

    this.helpers({
      password() {
        return this.password;
      },
      error() {
        if (_.isUndefined(this.error)) return '';
        return this.error;
      },
      message() {
        if (_.isUndefined(this.message)) return '';
        return this.message;
      },
      currentUser() {
        return _.isUndefined(Meteor.user()) ? {} : Meteor.user();
      },
    });
  }

  // Save profile (add or update)
  save() {
    this.message = '';
    this.error = '';

  // Create profile
    if (_.isUndefined(this.user.profile)) {
      this.user.profile = {
        locale: 'en',
        admin: false,
      };
    }

    // Workshops
    if (!_.isUndefined(this.user.profile.workshops)) {
      const realWorkshops = [];
      _.forEach(this.user.profile.workshops, (elem) => {
        realWorkshops.push(elem.text);
      });
      this.user.profile.workshops = realWorkshops;
    }

    // Update or insert
    if (this.userId !== '' && !_.isUndefined(this.userId)) {
      // Password
      if (this.password !== '' && this.userId !== '') {
        Meteor.call('users.setPassword', this.userId, this.password);
        this.message = 'Password updated.';
      }

      Meteor.call('users.update', this.userId, this.user, (err) => {
        if (err) this.error = err.message;
        if (!err) this.message = 'Successful operation.';
      });
    } else if (Meteor.user().profile.admin) {
      Accounts.createUser(_.extend(this.user, { password: this.password }), (err) => {
        if (err) this.error = err.message;
        if (!err) this.message = 'Successful operation.';
        this.state.transitionTo('users.edit');
      });
    }
  }
}

export default angular.module(name, [
  angularMeteor,
  uiRouter,
]).component(name, {
  template,
  controller: ['$scope', '$state', '$stateParams', '$document', '$translate', UsersEditCtrl],
});
