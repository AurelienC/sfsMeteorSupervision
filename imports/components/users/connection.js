import angular from 'angular';
import angularMeteor from 'angular-meteor';

import template from './connection.html';

export const name = 'usersConnection';

class UsersConnectionCtrl {
  constructor($scope, $document) {
    $scope.viewModel(this);

    /* eslint-disable */
    $document[0].title = `Supervision`;
    /*eslint-enable */

    this.helpers({
    });
  }

}


export default angular.module(name, [
  angularMeteor,
]).component(name, {
  template,
  controller: ['$scope', '$document', UsersConnectionCtrl],
});
