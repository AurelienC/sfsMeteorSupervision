import { Meteor } from 'meteor/meteor';
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import template from './list.html';

export const name = 'usersList';

class UsersListCtrl {
  constructor($scope, $state, $document, $translate) {
    $scope.viewModel(this);

    if (_.isUndefined(Meteor.user()) || !Meteor.user().profile.admin) {
      $state.transitionTo('users.connection');
    }

    this.subscribe('usersData');

    /* eslint-disable */
    $document[0].title = `Supervision`;
    /*eslint-enable */

    this.translate = $translate;

    this.helpers({
      users() {
        return Meteor.users.find();
      },
      error() {
        if (_.isUndefined(this.error)) return '';
        return this.error;
      },
      message() {
        if (_.isUndefined(this.message)) return '';
        return this.message;
      },
    });
  }

  // Delete user
  remove(userId) {
    this.error = '';
    this.message = '';
    Meteor.call('users.remove', userId, (err) => {
      if (err) this.error = err.message;
      if (!err) this.message = 'Successful operation.';
    });
  }
}

export default angular.module(name, [
  angularMeteor,
  uiRouter,
]).component(name, {
  template,
  controller: ['$scope', '$state', '$document', '$translate', UsersListCtrl],
});
