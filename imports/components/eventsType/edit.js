import { Meteor } from 'meteor/meteor';
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import { EventsType } from '../../api/eventsType/eventsType';

import { icons } from './icons';

import template from './edit.html';

export const name = 'eventsTypeEdit';

class EventsTypeEditCtrl {
  constructor($scope, $state, $document, $translate) {
    $scope.viewModel(this);

    if (_.isUndefined(Meteor.user()) || !Meteor.user().profile.admin) {
      $state.transitionTo('users.connection');
    }

    this.subscribe('eventsType');

    /* eslint-disable */
    $document[0].title = `Supervision`;
    /*eslint-enable */

    this.translate = $translate;
    this.state = $state;
    this.icons = icons;

    this.helpers({
      error() {
        if (_.isUndefined(this.error)) return '';
        return this.error;
      },
      title() {
        if (_.isUndefined(this.title)) return '';
        return this.title;
      },
      message() {
        if (_.isUndefined(this.message)) return '';
        return this.message;
      },
      events() {
        return EventsType.find();
      },
    });
  }

  // Open modal for edit an event
  edit(e) {
    this.eventToEdit = _.clone(e);
    this.title = this.translate.instant('Edit');
    angular.element('#itemEditModal').modal('show');
  }

// Open modal for add a new event
  add() {
    this.eventToEdit = {
      icon: '',
    };
    this.title = this.translate.instant('Add');
    angular.element('#itemEditModal').modal('show');
  }

  // Open icon's modal
  openIcons(icon) {
    const params = icon.split(' ');
    this.tempIcon = params[0];
    this.iconParam = params[1] || '';
    angular.element('#itemIcons').modal('show');
  }

  // Change the icon
  changeIcon(icon) {
    this.tempIcon = icon;
  }

// Valdiate the change
  validateIcon() {
    this.eventToEdit.icon = `${this.tempIcon}${(this.iconParam !== '') ? ` ${this.iconParam}` : ''}`;

    angular.element('#itemIcons').modal('hide');
  }

  // Save event (add or edit)
  save() {
    this.error = '';
    this.message = '';
    this.eventToEdit.oeeId = Number(this.eventToEdit.oeeId);
    Meteor.call('eventsType.upsert', this.eventToEdit._id, _.omit(this.eventToEdit, ['_id', '$$hashKey']), (err) => {
        if (err) {
          this.error = err.message;
        } else {
          angular.element('#itemEditModal').modal('hide');
          angular.element('#itemIcons').modal('hide');
          this.message = 'Successful operation.';
        }
    });
  }

// Remove an event
  remove(id) {
    this.message = '';
    Meteor.call('eventsType.remove', id, (err) => {
      if (!err) this.message = 'Successful operation.';
    });
  }

}

export default angular.module(name, [
  angularMeteor,
  uiRouter,
]).component(name, {
  template,
  controller: ['$scope', '$state', '$document', '$translate', EventsTypeEditCtrl],
});
