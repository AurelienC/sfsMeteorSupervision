import { moment } from 'meteor/momentjs:moment';
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import 'ng-tags-input';

import { Events } from '../../api/events/events';
import { EventsType } from '../../api/eventsType/eventsType';
import { Equipments } from '../../api/equipments/equipments';

import template from './list.html';

export const name = 'equipmentsList';

class EquipmentsListCtrl {
  constructor($scope, $document, $translate) {
    $scope.viewModel(this);
    moment.locale($translate.use());

    this.subscribe('equipments');
    this.subscribe('events');
    this.subscribe('eventsType');

    /* eslint-disable */
    $document[0].title = `Supervision`;
    /*eslint-enable */

    this.translate = $translate;
    this.eventTypeFilter = [];

    this.helpers({
      equipments() {
        return Equipments.find({}, {
          fields: {
            _id: 1,
            name: 1,
            number: 1,
            part: 1,
            active: 1,
            workshops: 1,
            state: 1,
            'settings.displayBadParts': 1,
            'settings.displayCountdown': 1,
          },
        });
      },

      eventsType() {
        return EventsType.find({}, {
          fields: {
            _id: 1,
            name: 1,
            icon: 1,
          },
        });
      },

    });
  }

  // Change filter (by state)
  changeFilters(id) {
    if (angular.element(`input[name=${id}]`).is(':checked')) {
      this.eventTypeFilter.push(id.toString());
    } else {
      this.eventTypeFilter.splice(this.eventTypeFilter.indexOf(id.toString()));
    }
  }

  // Calculation of the start of PO
  startOfPo(equipment) {
    const res = Events.findOne({
      equipmentId: equipment._id,
      po: equipment.state.po,
    }, {
      sort: {
        start: 1,
      },
      fields: {
        start: 1,
      },
    });

    return res || '';
  }
}

export default angular.module(name, [
  angularMeteor,
  uiRouter,
  'ngTagsInput',
]).component(name, {
  template,
  controller: ['$scope', '$document', '$translate', EquipmentsListCtrl],
})
/* eslint-disable */
.filter('po', () => {
  return (arg) => {
    if (_.isUndefined(arg)) return null;
    return arg.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }
})
.filter('workshops', () => {
  return (arg, separator) => {
    if (_.isUndefined(separator) || _.isUndefined(arg)) return null;
    let str = '';
    _.each(arg, (element) => {
      str += `${separator}${element}`;
    });
    return str;
  }
})
.filter('workshopsFilter', () => {
  return (list, filter) => {
    if (_.isUndefined(list) || _.isUndefined(filter)) return list;
    if (_.isUndefined(filter) || filter.length === 0) return list;

    let listOfFilters = [];
    let result = [];

    _.each(filter, (value) => {
      listOfFilters.push(value.text);
    });

    _.forEach(list, (value) => {
      if (_.intersection(listOfFilters, value.workshops).length > 0) {
        result.push(value);
      }
    });
    return result;
  };
})
.filter('eventsTypeFilter', () => {
  return (list, eventTypeFilter) => {
    if (_.isUndefined(list) || _.isUndefined(eventTypeFilter)) return list;
    let result = [];
    _.forEach(list, (value) => {
      if (eventTypeFilter.length === 0 || eventTypeFilter.indexOf(value.state.eventTypeId) >= 0) {
        result.push(value);
      }
    })
    return result;
  }
});
/* eslint-enable */
