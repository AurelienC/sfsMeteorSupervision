import { Meteor } from 'meteor/meteor';
import { moment } from 'meteor/momentjs:moment';
import { ReactiveVar } from 'meteor/reactive-var';
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import template from './analyze.html';

export const name = 'equipmentsAnalyze';

class EquipmentsAnalyzeCtrl {
  constructor($scope, $state, $document, $translate) {
    $scope.viewModel(this);

    if (_.isUndefined(Meteor.userId()) || !Meteor.userId()) {
      $state.transitionTo('users.connection');
    }

    /* eslint-disable */
    $document[0].title = `${$translate.instant('Analyze')} - Supervision`;
    /*eslint-enable */

    this.subscribe('events');
    this.state = $state;
    this.translate = $translate;
    this.startQuery = new ReactiveVar(moment()
    .hour(0)
    .minute(0)
    .second(0)
    .millisecond(0)
    .subtract(1, 'month'));
    this.endQuery = new ReactiveVar(moment());
    this.otherPeriodStart = this.startQuery.get();
    this.otherPeriodEnd = this.endQuery.get();

    // Initialization
    this.createGrid();
    this.initGrid();

    this.helpers({
    });
  }

  // Periods
  period() {
    angular.element('#rangeChoice').modal('show');
  }

  validatePeriod() {
    this.startQuery.set(this.otherPeriodStart);
    this.endQuery.set(this.otherPeriodEnd);
    angular.element('#rangeChoice').modal('hide');
    this.initGrid();
  }

  // Initialize grid (table)
  initGrid() {
    const startQuery = moment(this.startQuery.get()).toDate();
    const endQuery = moment(this.endQuery.get()).toDate();

    Meteor.call('events.getForAnalyze', startQuery, endQuery, (err, d) => {
      if (!err) {
        this.events = d;
        angular.element('#grid').jqGrid('setGridParam', { datatype: 'local', data: d }).trigger('reloadGrid');
      }
    });
  }

  // Create grid (table)
  createGrid() {
    let groupingOptions = ['equipment.name', '_id.po'];
    let oeeGroup = {
      oee: 0,
      good: 0,
      bad: 0,
      1: 0,
      2: 0,
      3: 0,
      4: 0,
      10: 0,
    };
    angular.element('#grid').jqGrid({
      iconSet: 'fontAwesome',
      datatype: 'local', // Datatype
      // data,
      colModel: [
        {
          label: this.translate.instant('Actions'),
          name: 'equipment._id',
          hidden: true,
        }, {
          name: '_id.po',
          label: this.translate.instant('PO'),
          formatter: 'integer',
          formatoptions: { thousandsSeparator: '.' },
          searchoptions: {
            searchOperMenu: true,
            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge'],
          },
        }, {
          name: '_id.article',
          label: this.translate.instant('Article'),
          sorttype: 'number',
          searchoptions: {
            searchOperMenu: true,
            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge'],
          },
        }, {
          label: this.translate.instant('OEE'),
          sorttype: 'number',
          formatter: (cellValue) => {
            if (_.isUndefined(cellValue)) {
              return '';
            }
            return `<b>${cellValue}%</b>`;
          },
          summaryType: (val, nameF, record) => {
            if (val === '') {
              oeeGroup = {
                oee: 0,
                good: 0,
                bad: 0,
                1: 0,
                2: 0,
                3: 0,
                4: 0,
                10: 0,
              };
            }
            oeeGroup.good += record.good;
            oeeGroup.bad += record.bad;
            oeeGroup[record['_id.oeeId']] += record['_id.oeeId'];
            oeeGroup.oee = oeeGroup[10] / (oeeGroup[2] + oeeGroup[3] + oeeGroup[4] + oeeGroup[10]);

            return Math.trunc(100 * oeeGroup.oee);
          },
        }, {
          name: 'equipment.name',
          label: this.translate.instant('Equipment'),
          searchoptions: {
            searchOperMenu: true,
            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge', 'bw', 'bn', 'in', 'ni', 'ew', 'en', 'cn', 'nc', 'nu', 'nn'],
          },
        }, {
          name: '_id.oeeId',
          label: this.translate.instant('OEE type'),
          sorttype: 'integer',
          edittype: 'select',
          formatter: 'select',
          editoptions: { value: this.translate.instant('OEEoptions') },
          stype: 'select',
          searchoptions: {
            value: this.translate.instant('OEEoptions'),
            searchOperMenu: true,
            defaultValue: this.translate.instant('All'),
            noFilterText: this.translate.instant('All'),
            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge', 'bw', 'bn', 'in', 'ni', 'ew', 'en', 'cn', 'nc', 'nu', 'nn'],
          },
        }, {
          name: '_id.information',
          label: this.translate.instant('Information'),
          searchoptions: {
            searchOperMenu: true,
            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge', 'bw', 'bn', 'in', 'ni', 'ew', 'en', 'cn', 'nc', 'nu', 'nn'],
          },
        }, {
          name: 'duration',
          label: this.translate.instant('Duration'),
          formatter: cellValue => this.durationDisplay(cellValue),
        }, {
          name: 'good',
          label: this.translate.instant('Good'),
          sorttype: 'integer',
          summaryTpl: '{0}',
          summaryType: 'sum',
          formatter: 'integer',
          formatoptions: { thousandsSeparator: ' ' },
          searchoptions: {
            searchOperMenu: true,
            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge'],
          },
        }, {
          name: 'bad',
          label: this.translate.instant('Bad'),
          sorttype: 'integer',
          summaryTpl: '{0}',
          summaryType: 'sum',
          formatter: 'integer',
          formatoptions: { thousandsSeparator: ' ' },
          searchoptions: {
            searchOperMenu: true,
            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge'],
          },
        }, {
          name: 'rate',
          label: this.translate.instant('Rate'),
          sorttype: 'integer',
          summaryTpl: '{0}',
          summaryType: 'avg',
          formatter: 'integer',
          formatoptions: { thousandsSeparator: ' ' },
          searchoptions: {
            searchOperMenu: true,
            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge', 'bw', 'bn', 'in', 'ni', 'ew', 'en', 'cn', 'nc', 'nu', 'nn'],
          },
        }],
        rowNum: 20,
        rowList: [10, 20, 30, 40, 50, '10000:All'],
        pager: '#pager',
        multiSort: true,
        viewrecords: true,
        sortable: true,
        caption: this.translate.instant('Analyze'),
        height: '100%',
        gridview: true,
        ignoreCase: true,
        autoencode: true,
        autowidth: true,
        searching: {
          showQuery: true,
          loadFilterDefaults: false,
          multipleSearch: true,
          multipleGroup: true,
          closeOnEscape: true,
          searchOperators: true,
          searchOnEnter: true,
        },
        grouping: true,
        groupingView: {
          groupField: groupingOptions,
          groupColumnShow: [true, true],
          groupText: [`<b>{0} <small>{1} ${this.translate.instant('line(s)')}</small></b>`, `<b>{0} <small>{1} ${this.translate.instant('line(s)')}</small></b>`],
          groupOrder: ['asc', 'asc'],
          groupSummary: [true, true],
          groupCollapse: true,
          groupSummaryPos: ['header', 'header'],
        },
      });

      angular.element('#grid').navGrid('#pager', {
        search: true,
        add: false,
        edit: false,
        del: false,
        refresh: false,
      },
      {}, // edit options
      {}, // add options
      {}, // delete options
      {
        multipleSearch: true,
        multipleGroup: true,
        showQuery: true,
      });

      angular.element('#grid').jqGrid('filterToolbar', {
        stringResult: true,
        searchOperators: true,
      });

      angular.element('#grid').navButtonAdd('#pager', {
        buttonicon: 'fa fa-info-circle',
        title: this.translate.instant('Detail of selected line'),
        caption: this.translate.instant('Detail of selected line'),
        position: 'last',
        onClickButton: () => {
          const rowValue = angular.element('#grid').jqGrid('getRowData', angular.element('#grid').jqGrid('getGridParam', 'selrow'));
          if (!_.isArray(rowValue)) {
            this.state.transitionTo('equipments.view', { equipmentId: rowValue['equipment._id'], po: rowValue['_id.po'], article: rowValue['_id.article'] });
          }
        },
      });

      angular.element('#chngroup').change(() => {
        const vl = angular.element('#chngroup').val();
        if (vl) {
          if (vl === 'clear') {
            angular.element('#grid').jqGrid('groupingRemove', true);
            angular.element('#chngroup2').hide();
          } else {
            groupingOptions[0] = vl;
            angular.element('#grid').jqGrid('groupingGroupBy', groupingOptions);
            angular.element('#chngroup2').show();
          }
        }
      });

      angular.element('#chngroup2').change(() => {
        const vl = angular.element('#chngroup2').val();
        if (vl) {
          if (vl === 'clear') {
            const save = groupingOptions[0];
            groupingOptions = [];
            groupingOptions[0] = save;
            angular.element('#grid').jqGrid('groupingGroupBy', groupingOptions);
          } else {
            groupingOptions[1] = vl;
            angular.element('#grid').jqGrid('groupingGroupBy', groupingOptions);
          }
        }
      });
    }

    // Display duration into table
    durationDisplay(arg) {
      if (_.isUndefined(arg)) return null;

      function d(v) {
        return (v < 10) ? `0${v}` : v;
      }

      const h = moment.duration(arg, 'milliseconds').asHours();
      const H = Math.trunc(h);
      const m = (h - H) * 60;
      const M = Math.trunc(m);
      const s = Math.trunc((m - M) * 100);

      return ((H > 0) ? `${H}h` : '') + ((H > 0) ? `${d(M)}m` : ((M > 0) ? `${d(M)}m` : '')) + ((s > 0) ? `${d(s)}s` : '');  // eslint-disable-line
    }

    // Export data
    exportToCsv() {
      const values = [];

      _.each(this.events, (elem) => {
        values.push(
          _.omit(
            _.extend(elem, {
              po: elem._id.po || '',
              article: elem._id.article || '',
              oeeId: elem._id.oeeId,
              information: elem._id.information || '',
              equipment: elem.equipment.name,
              duration: Math.trunc(elem.duration / 1000),
              rate: Math.trunc(elem.rate),
            }), ['_id', 'id']));
          });

          Meteor.call('events.toCsv', values, (err, fileContent) => {
            if (fileContent) {
              const blob = new Blob([fileContent], { type: 'text/csv;charset=utf-8' }); // eslint-disable-line
              saveAs(blob, 'analyze.csv');  // eslint-disable-line
            }
          });
        }
      }

      export default angular.module(name, [
        angularMeteor,
        uiRouter,
      ]).component(name, {
        template,
        controller: ['$scope', '$state', '$document', '$translate', EquipmentsAnalyzeCtrl],
      });
