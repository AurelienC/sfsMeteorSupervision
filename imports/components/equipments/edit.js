import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import { Equipments } from '../../api/equipments/equipments';

import template from './edit.html';

export const name = 'equipmentsEdit';

class EquipmentsEditCtrl {
  constructor($scope, $state, $stateParams, $document, $translate) {
    $scope.viewModel(this);

    if (_.isUndefined(Meteor.user()) || Meteor.user().profile.admin !== true) {
      $state.transitionTo('users.connection');
    }

    this.state = $state;
    this.translate = $translate;

    /* eslint-disable */
    $document[0].title = `Supervision`;
    /*eslint-enable */

    this.subscribe('equipments');

    // Retrieve parameters
    this.equipmentId = $stateParams.equipmentId;
    if (this.equipmentId !== '') {
      this.eqpt = Equipments.findOne({
      _id: $stateParams.equipmentId }, {
        fields: {
          state: 0,
          informations: 0,
        },
      });
    }

    this.helpers({
      error() {
        if (_.isUndefined(this.error)) return '';
        return this.error;
      },
      message() {
        if (_.isUndefined(this.message)) return '';
        return this.message;
      },
      e() {
        if (this.equipmentId === '') {
          return Equipments.schema.clean({});
        }
        return this.eqpt;
      },
      informations() {
        if (this.equipmentId === '') {
          return null;
        }
        return Equipments.findOne({
          _id: $stateParams.equipmentId,
        }, {
          fields: { informations: 1 },
        });
      },
    });
  }

  // Add information to equipment
  addInformation(newInformation) {
    this.clearMessages();

    if (_.isUndefined(newInformation.activeOee)) _.extend(newInformation, { activeOee: false });

    if (!newInformation.activeOee) {
      _.omit(newInformation, 'oeeId');
    }

    if (!_.isNumber(Number(newInformation.oeeId))) {
      this.error = 'Choose an OEE type.';
    } else {
      Meteor.call('equipments.addInformation', this.equipmentId, newInformation, (err) => {
        if (err) this.error = err.message;
      });
      this.newInformation = {};
    }
  }

  // Remove information to equipment
  removeInformation(information) {
    this.clearMessages();
    Meteor.call('equipments.removeInformation', this.equipmentId, information.id, (err) => {
      if (err) this.error = err.message;
    });
  }

  // Clear messages (message successful and error)
  clearMessages() {
    this.error = '';
    this.message = '';
  }

  // Save equipment
  saveEquipment(eqt) {
    let equipment = eqt;
    this.clearMessages();
    const realWorkshops = [];
    if (_.isUndefined(equipment)) this.equipment = {};

    _.forEach(equipment.workshops, (elem) => {
      realWorkshops.push(elem.text);
    });
    _.extend(equipment, { workshops: realWorkshops });
    equipment = _.omit(equipment, ['$hashKey', 'informations']);

    try {
      equipment.settings.runningMode = Number(equipment.settings.runningMode);
      equipment.settings.countingMode = Number(equipment.settings.countingMode);
      console.log(equipment);

      check(equipment, Equipments.schema);

      if (this.equipmentId === '') {
        Meteor.call('equipments.insert', equipment, (err, res) => {
          if (err) this.error = err.message;
          if (res) this.state.transitionTo('equipments.view', { equipmentId: res });
        });
      } else {
        Meteor.call('equipments.update', equipment._id, _.omit(equipment, ['informations', 'state']), (err) => {
          if (err) this.error = err.message;
          if (!err) this.message = 'Equipment saved.';
        });
      }
    } catch (e) {
      this.error = e.message;
    }
  }

  // Open modal transfer paramaters
  modalTransfer() {
    angular.element('#modalTransfer').modal('show');
  }

  // Open modal delete events/equipments
  modalDelete() {
    angular.element('#modalDelete').modal('show');
  }

  // Export all events about this equipment
  exportToCsv() {
    this.clearMessages();
    Meteor.call('events.allToCsv', this.equipmentId, (err, fileContent) => {
      if (err) this.error = err.message;
      if (!err) this.message = 'Export success.';
      if (fileContent && !err) {
        const blob = new Blob([fileContent], { type: 'text/csv;charset=utf-8' }); // eslint-disable-line
        saveAs(blob, 'events.csv');  // eslint-disable-line
      }
    });
  }

  // Delete all events of an equipment
  deleteEvents() {
    this.clearMessages();
    angular.element('#modalDelete').modal('hide');
    Meteor.call('equipments.removeEvents', this.equipmentId, (err) => {
      if (err) this.error = err.message;
      if (!err) this.message = 'Events deleted.';
    });
  }

  // Delete equipment and all events of this equipment
  deleteEquipment() {
    angular.element('#modalDelete').modal('hide');
    this.clearMessages();
    Meteor.call('equipments.remove', this.equipmentId, (err) => {
      if (err) this.error = err.message;
      if (!err) this.state.transitionTo('equipments.list');
    });
  }

  // Send settings to PLC
  transfer() {
    this.clearMessages();
    Meteor.call('equipments.sendParameters', this.equipmentId, (err) => {
      if (err) this.error = err.message;
      if (!err) this.message = 'Request for parameter transfer.';
    });
    angular.element('#modalTransfer').modal('hide');
  }

  // Cancel send settings to PLC
  cancelTransfer() {
    this.clearMessages();
    Meteor.call('equipments.cancelParameters', this.equipmentId, (err) => {
      if (err) this.error = err.message;
      if (!err) this.message = 'Request canceled.';
    });
  }
}

export default angular.module(name, [
  angularMeteor,
  uiRouter,
]).component(name, {
  template,
  controller: ['$scope', '$state', '$stateParams', '$document', '$translate', EquipmentsEditCtrl],
});
