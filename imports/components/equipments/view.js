import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { moment } from 'meteor/momentjs:moment';
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import angularMoment from 'angular-moment';
import uiRouter from 'angular-ui-router';
import vis from 'vis';
import Chart from 'chart.js';
import 'angular-bootstrap-datetimepicker';

import { Events } from '../../api/events/events';
import { EventsType } from '../../api/eventsType/eventsType';
import { Equipments } from '../../api/equipments/equipments';
import Oee from '../../api/events/oeeCalculation';

import template from './view.html';

export const name = 'equipmentsView';


class EquipmentsViewCtrl {
  constructor($scope, $state, $stateParams, $document, $translate) {
    $scope.viewModel(this);

    // Subscriptions
    this.subscribe('events');
    this.subscribe('eventsType');
    this.subscribe('equipments');


    // Access level
    this.equipment = Equipments.findOne({ _id: $stateParams.equipmentId }, { fields: {
      name: 1,
      settings: 1,
      rate: 1,
    } });

    if (_.isUndefined(this.equipment)) {
      $state.transitionTo('users.connection');
    }

    // Retrieve parameters
    this.translate = $translate;
    this.equipmentId = $stateParams.equipmentId;
    this.po = new ReactiveVar($stateParams.po);
    this.article = new ReactiveVar($stateParams.article);
    this.startQuery = new ReactiveVar(moment()
    .hour(0)
    .minute(0)
    .second(0)
    .millisecond(0));
    this.endQuery = new ReactiveVar(moment());
    this.otherPeriodStart = this.startQuery.get();
    this.otherPeriodEnd = this.endQuery.get();


    this.eventsCount = 0;
    this.events = [];
    this.oee = {};
    this.eventToEdit = {};
    this.errorMessage = '';
    this.displayErrorMessage = false;
    this.periods = [{
      id: 0,
      name: this.translate.instant('Yesterday'),
      start: new Date(new Date().setDate(new Date().getDate() - 1)).setHours(0, 0, 0),
      end: new Date(new Date().setDate(new Date().getDate() - 1)).setHours(23, 59, 59),
      class: '',
    }, {
      id: 1,
      name: this.translate.instant('Today'),
      start: new Date().setHours(0, 0, 0),
      end: new Date().setHours(23, 59, 59),
      class: 'active',
    }, {
      id: 2,
      name: this.translate.instant('9am-5pm'),
      start: new Date(new Date().setDate(new Date().getDate() - 1)).setHours(21, 0, 0),
      end: new Date().setHours(5, 0, 0),
      class: '',
    }, {
      id: 3,
      name: this.translate.instant('5pm-1am'),
      start: new Date().setHours(5, 0, 0),
      end: new Date().setHours(13, 0, 0),
      class: '',
    }, {
      id: 4,
      name: this.translate.instant('1am-9pm'),
      start: new Date().setHours(13, 0, 0),
      end: new Date().setHours(21, 0, 0),
      class: '',
    }, {
      id: 5,
      name: this.translate.instant('9pm-5am'),
      start: new Date().setHours(21, 0, 0),
      end: new Date(new Date().setDate(new Date().getDate() + 1)).setHours(5, 0, 0),
      class: '',
    }];


    // Title of page
    if (!_.isUndefined(this.equipment)) {
      $document[0].title = `${this.equipment.name} - Supervision`; // eslint-disable-line
    } else {
      $document[0].title = 'Supervision'; // eslint-disable-line
    }


    angular.element('[data-toggle="tooltip"]').tooltip();
    angular.element('[data-toggle="dropdown"]').dropdown();
    this.initCharts();

    this.helpers({
      events() {
        const po = String(this.po.get());
        const article = String(this.article.get());

        // List of events
        const parametersQuery = {
          equipmentId: this.equipmentId,
        };

        if (!_.isEmpty(po)) {
          _.extend(parametersQuery, { po: Number(po) });
        }

        if (!_.isEmpty(article)) {
          _.extend(parametersQuery, { article: Number(article) });
        }

        if (_.isEmpty(po) && _.isEmpty(article)) {
          const startQuery = moment(this.startQuery.get()).toDate();
          const endQuery = moment(this.endQuery.get()).toDate();

          _.extend(parametersQuery, { $or: [
            { $and: [{ start: { $gte: startQuery } }, { start: { $lte: endQuery } }] },
            { $and: [{ end: { $gte: startQuery } }, { end: { $lte: endQuery } }] },
            { $and: [
              { start: { $lte: startQuery } },
              { start: { $lte: endQuery } },
              { end: { $gte: startQuery } },
              { end: { $gte: endQuery } },
            ] },
          ],
        });
      } else {
        // Change start and end date when PO or article is selected
        const eventForStartDate = Events.findOne(parametersQuery, {
          sort: { start: 1 },
          fields: { start: 1 } });
          const eventForEndDate = Events.findOne(parametersQuery, {
            sort: { end: -1 },
            fields: { end: 1 } });

            if (!_.isUndefined(eventForStartDate) && !_.isUndefined(eventForEndDate)) {
              this.startQuery.set(eventForStartDate.start);
              this.endQuery.set(eventForEndDate.end);
            }
          }
          const events = Events.find(parametersQuery, {
            sort: {
              end: -1,
            },
          });

          this.eventsCount = events.count();
          const infoArray = [];

          const oee = Oee.oeeCalculation({
            start: moment(this.startQuery.get()).toDate(),
            end: moment(this.endQuery.get()).toDate(),
            events,
            rate: _.isUndefined(this.equipment) ? 0 : this.equipment.rate,
          });

          const grouped = _.groupBy(events.fetch(), elem => elem.information);

          _.each(grouped, (elem) => {
            let total = 0;
            let qty = 0;
            _.each(elem, (elem2) => {
              if (!elem2.secondary) {
                total += moment.duration(moment(elem2.end).diff(elem2.start, 'seconds'), 'seconds').asSeconds();
                qty += 1;
              }
            });
            infoArray.push({
              information: elem[0].information,
              total,
              avg: total / qty,
              qty,
            });
          });

          this.events = events.fetch();
          this.oee = oee;

          this.updateCharts();

          return {
            events: events.fetch(),
            oee,
            informations: infoArray,
          };
        },
        eventsType() {
          return EventsType.find({});
        },
        eventsCount() {
          return this.eventsCount;
        },
        equipment() {
          return Equipments.findOne({
            _id: this.equipmentId,
          });
        },
        error() {
          if (_.isUndefined(this.error)) return '';
          return this.error;
        },
        message() {
          if (_.isUndefined(this.message)) return '';
          return this.message;
        },
        errorEvent() {
          if (_.isUndefined(this.errorEvent)) return '';
          return this.errorEvent;
        },
        user() {
          return Meteor.user();
        },
      });
    }

    clearPeriod() {
      _.each(this.periods, (elem, index) => {
        this.periods[index].class = '';
      });
      angular.element('#other').addClass('active');
    }

    setPeriod(p) {
      this.startQuery.set(p.start);
      this.endQuery.set(p.end);

      _.each(this.periods, (elem, index) => {
        if (elem.id === p.id) {
          this.periods[index].class = 'active';
        } else {
          this.periods[index].class = '';
        }
      });
      angular.element('#other').removeClass('active');
    }

    otherPeriod() {
      angular.element('#rangeChoice').modal('show');
    }

    validateOtherPeriod() {
      this.startQuery.set(this.otherPeriodStart);
      this.endQuery.set(this.otherPeriodEnd);
    }

    openModal() {
      angular.element('#realStateModal').modal('show');
    }

    changePo(value) {
      this.po.set(value);

      if (this.po.get() === '' && this.article.get() === '') {
        this.setPeriod({
          start: moment()
          .hour(0)
          .minute(0)
          .second(0)
          .millisecond(0),
          end: moment(),
        });
      }
    }

    changeArticle(value) {
      this.article.set(value);

      if (this.po.get() === '' && this.article.get() === '') {
        this.setPeriod({
          start: moment()
          .hour(0)
          .minute(0)
          .second(0)
          .millisecond(0),
          end: moment(),
        });
      }
    }

    exportToCsv() {
      const values = [];

      _.each(this.events.events, (elem) => {
        values.push(
          _.extend(
            _.omit(elem, ['_id', 'color', 'equipmentId', 'icon', '$$hashKey', 'information']), {
              equipment: this.equipment.name,
              information: elem.information || '',
              start: moment(elem.start).format('DD/MM/YYYY hh:mm:ss'),
              end: moment(elem.end).format('DD/MM/YYYY hh:mm:ss'),
              rate: elem.rate,
            }));
          });

          Meteor.call('events.toCsv', values, (err, fileContent) => {
            if (fileContent) {
              const blob = new Blob([fileContent], { type: 'text/csv;charset=utf-8' }); // eslint-disable-line
              saveAs(blob, 'events.csv');  // eslint-disable-line
            }
          });
        }

        clearMessage() {
          this.error = '';
          this.message = '';
          this.errorEvent = '';
        }

        addEvent() {
          this.clearMessage();
          angular.element('#itemEditModal').modal('show');
        }

        removeEvent(eventId) {
          Meteor.call('events.remove', eventId);
        }

        editEvent(event) {
          this.clearMessage();
          this.eventToEdit = _.clone(event);
          angular.element('#itemEditModal').modal('show');
        }

        saveEvent() {
          this.clearMessage();
          if (!moment(this.eventToEdit.end).isAfter(this.eventToEdit.start)) {
            this.errorEvent = 'The end date must be after the start date.';
          } else if (_.isUndefined(this.eventToEdit.name)) {
            this.errorMessage = 'You have to choose an event type.';
          } else if (_.isUndefined(this.eventToEdit.oeeId)) {
            this.errorMessage = 'You have to choose an OEE type.';
          } else {
            const eventType = EventsType.findOne({ name: this.eventToEdit.name }, {
              fields: {
                icon: 1,
                color: 1,
              } });

              _.extend(this.eventToEdit, {
                icon: eventType.icon,
                color: eventType.color,
                oeeId: Number(this.eventToEdit.oeeId),
              });

              if (_.isUndefined(this.eventToEdit._id)) {
                Meteor.call('events.insert', _.extend(this.eventToEdit, { equipmentId: this.equipmentId }), (err) => {
                  if (err) this.errorEvent = err.message;
                  if (!err) {
                    this.message = 'Successful operation.';
                    angular.element('#itemEditModal').modal('hide');
                    this.eventToEdit = {};
                  }
                });
              } else {
                Meteor.call('events.update', this.eventToEdit._id, _.omit(this.eventToEdit, '$$hashKey'), (err) => {
                  if (err) this.errorEvent = err.message;
                  if (!err) {
                    this.message = 'Successful operation.';
                    angular.element('#itemEditModal').modal('hide');
                    this.eventToEdit = {};
                  }
                });
              }
            }
          }


          setFilter(filter) {
            angular.element('#table-filter').val(filter.toString());
          }

          currentUser() {
            return Meteor.user();
          }

          getRate(start, end, good, bad) {
            if (!_.isUndefined(start) && !_.isUndefined(end)) {
              const r = Math.trunc(
                ((good || 0) + (bad || 0)) / (moment(end).diff(moment(start)) / 3600000),
              );
              angular.element('#rate').val(r);
              this.eventToEdit.rate = r;
            }
          }

          initCharts() {
            const domOeeChart = angular.element('#oeeChart');
            const domDurationsChart = angular.element('#durationsChart');
            const domTimeline = angular.element('#timeline');

            // Init oee chart
            this.oeeChart = new Chart(domOeeChart, {
              type: 'radar',
              data: {
                labels: [
                  this.translate.instant('Productivity'),
                  this.translate.instant('Availability'),
                  this.translate.instant('Performance'),
                  this.translate.instant('Quality'),
                ],
                datasets: [{
                  label: '%',
                }],
              },
              options: {
                responsive: true,
                legend: {
                  display: false,
                },
                scale: {
                  ticks: {
                    suggestedMin: 70,
                    stepSize: 10,
                    callback: value => `${Math.round(value)}%`,
                  },
                },
              },
            });

            // Init durations chart
            this.durationsChart = new Chart(domDurationsChart, {
              type: 'pie',
              data: {
                labels: [
                  this.translate.instant('Non-production period'),
                  this.translate.instant('Planned stop'),
                  this.translate.instant('Unexpected stop'),
                  this.translate.instant('Producing (bad)'),
                  this.translate.instant('Producing (good)'),
                ],
                datasets: [{
                  backgroundColor: [
                    'rgba(96, 96, 96, 0.4)',  // Grey
                    'rgba(255, 128, 0, 0.4)', // Orange
                    'rgba(204, 0, 0, 0.4)',   // Red
                    'rgba(255, 216, 0, 0.4)', // Yellow
                    'rgba(102, 204, 0, 0.4)', // Green
                  ],
                }],
              },
              options: {
                responsive: true,
                legend: {
                  display: true,
                },
                tooltips: {
                  enabled: true,
                  mode: 'point',
                  callbacks: {
                    label: (tooltipItems, data) => {
                      const idx = tooltipItems.index;
                      const value = data.datasets[0].data[idx];
                      const minutes = moment.duration(value, 'seconds').minutes() < 10 ? `0${moment.duration(value, 'seconds').minutes()}` : moment.duration(value, 'seconds').minutes();
                      return `${data.labels[idx]} : ${moment.duration(value, 'seconds').humanize()} (${moment.duration(value, 'seconds').hours()}h${minutes})`;
                    },
                  },
                },
              },
            });

            // Init timeline
            this.timeline = new vis.Timeline(domTimeline[0], new vis.DataSet(), {
              start: this.startQuery.get(),
              end: this.endQuery.get(),
              maxHeight: '500px',
              showCurrentTime: true,
              editable: false,
              selectable: true,
              multiselect: false,
              stack: false,
              zoomMin: 30 * 60 * 1000,
              tooltip: {
                followMouse: true,
              },
              template: item => `<i class="fa ${item.icon}" aria-hidden="true"></i>`,
            });

            // Events
            this.timeline.on('rangechanged', (prop) => {
              if (!(moment(this.startQuery.get()).isSame(new Date(prop.start)) &&
              moment(this.endQuery.get()).isSame(new Date(prop.end)))) {
                this.clearPeriod();

                this.startQuery.set(prop.start);
                this.endQuery.set(prop.end);
              }
            });
          }

          updateCharts() {
            // OEE chart
            this.oeeChart.config.data.datasets[0].data = [
              Math.round(this.oee.OEE * 100),
              Math.round(this.oee.availability * 100),
              Math.round(this.oee.performance * 100),
              Math.round(this.oee.quality * 100),
            ];
            this.oeeChart.update();

            // durations chart
            this.durationsChart.config.data.datasets[0].data = [
              // No production
              Math.abs(this.oee.totalOperationgTime - this.oee.plantOperatingTime), // eslint-disable-line
              // Planned stop
              Math.abs(this.oee.plantOperatingTime - this.oee.planedProductionTime), // eslint-disable-line
              // Unplaned stop
              Math.abs(this.oee.planedProductionTime - this.oee.operatingTime),
              // Under nominal rate
              Math.abs(this.oee.netOperatingTime - this.oee.fullyProductiveTime), // eslint-disable-line
              // Under production
              this.oee.fullyProductiveTime,
            ];
            this.durationsChart.update();

            // timeline chart
            this.eventsTimeline = [];
            this.events.forEach((event) => {
              this.eventsTimeline.push({
                title: event.name,
                start: event.start,
                end: event.end,
                className: event.color,
                icon: event.icon,
              });
            });

            this.timeline.setItems(new vis.DataSet(this.eventsTimeline));
            this.timeline.setOptions({
              start: moment(this.startQuery.get()),
              end: moment(this.endQuery.get()),
            });
          }

        }

        export default angular.module(name, [
          angularMeteor,
          angularMoment,
          uiRouter,
          'ui.bootstrap.datetimepicker',
        ])
        .component(name, {
          template,
          controller: ['$scope', '$state', '$stateParams', '$document', '$translate', EquipmentsViewCtrl],
        })
        /* eslint-disable */
        .filter('sumFilter', () => {
          return (collection, arg) => {
            if (_.isUndefined(collection) || _.isUndefined(arg)) return null;
            let total = 0;
            _.each(collection, (value) => {
              total += value[arg];
            });

            return total;
          };
        })
        .filter('avgFilter', () => {
          return (collection, arg) => {
            if (_.isUndefined(collection) || _.isUndefined(arg)) return null;
            let total = 0;
            _.each(collection, (value) => {
              total += value[arg];
            });

            return total / collection.length;
          };
        })
        .filter('po', () => {
          return (arg) => {
            if (_.isUndefined(arg)) return null;
            return arg.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
          }
        })
        .filter('percentage', () => {
          return (arg) => {
            if (_.isUndefined(arg)) return null;
            return arg * 100;
          }
        })
        .filter('qtyFilter', () => {
          return (collection, arg) => {
            if (_.isUndefined(collection) || _.isUndefined(arg)) return null;
            const result = [];
            _.each(collection, (element) => {
              if(element[arg] !== 0) result.push(element[arg]);
            });
            return _.uniq(result).length;
          }
        })
        .filter('duration', () => {
          return (arg) => {
            if (_.isUndefined(arg)) return null;

            if(arg < 60) {
              return moment.utc(arg * 1000).format('s[s]');
            } else if (arg < 3600) {
              return moment.utc(arg * 1000).format('m[m] ss[s]');
            }
              return moment.utc(arg * 1000).format('h[h] mm[m]');
          }
        })
        .filter('countdown', () => {
          return (arg) => {
            if (_.isUndefined(arg)) return null;
            return arg < 1 ? '> 10' : arg * 10;
          }
        });
        /* eslint-enable */
