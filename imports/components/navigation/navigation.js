import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import j from 'jwerty';
import angular from 'angular';
import angulari18n from 'angular-i18n/angular-locale_fr-fr'; // eslint-disable-line no-unused-vars
import angularMeteor from 'angular-meteor';

import { Equipments } from '../../api/equipments/equipments';


import template from './navigation.html';

export const name = 'navigation';

class Navigation {
  constructor($scope, $location, $translate) {
    $scope.viewModel(this);

    this.subscribe('equipments');

    angular.element('.dropdown-toggle').dropdown();
    this.errorMessage = '';
    this.displayErrorMessage = false;
    this.translate = $translate;
    this.location = $location;

    // Keyboard shortcut
    j.jwerty.key('F1', () => {
      $scope.$apply(() => {
        $location.path('/equipments/list');
      });
    });

    // Keyboard shortcut
    j.jwerty.key('F2', () => {
      $scope.$apply(() => {
        $location.path('/equipments/analyze');
      });
    });

    this.helpers({
      errorMessage() {
        return this.errorMessage;
      },
      displayErrorMessage() {
        return this.displayErrorMessage;
      },
      equipments() {
        return Equipments.find({}, {
          fields: {
            _id: 1,
            name: 1,
            active: 1,
            'state.icon': 1,
          },
        });
      },
      currentUser() {
        return Meteor.user();
      },
    });
  }

  // Open connection modal
  openModal() {
    this.username = '';
    this.password = '';
    this.displayErrorMessage = false;
    angular.element('#connectionModal').modal();
  }

  connection(username, password) {
    try {
      this.displayErrorMessage = false;
      check(username, String);
      check(password, String);

      Meteor.loginWithPassword(username, password, (error) => {
        if (error) {
          this.errorMessage = 'Login error: The user does not exist or the password is incorrect.';
          this.displayErrorMessage = true;
        } else {
          angular.element('#connectionModal').modal('hide');
          this.translate.use(Meteor.user().profile.locale || 'fr');
        }
      });
    } catch (e) {
      if (e.errorType === 'Match.Error') {
        this.errorMessage = 'Error: The username and password must be entered.';
      } else {
        this.errorMessage = e.message;
      }
      this.displayErrorMessage = true;
    }
  }

  logout() {
    Meteor.logout();
    this.location.path('/equipments/list');
  }

  setLang(langKey) {
    this.translate.use(langKey);
    if (!_.isNull(Meteor.userId())) {
      Meteor.call('users.changeLocale', Meteor.userId(), langKey);
    }
  }

  getLang() {
    return this.translate.use();
  }
}

export default angular.module(name, [
  angularMeteor,
  'pascalprecht.translate',
]).component(name, {
  template,
  controller: ['$scope', '$location', '$translate', Navigation],
})
.config(['$translateProvider', ($translateProvider) => {
  $translateProvider.useStaticFilesLoader({
    prefix: 'i18n/',
    suffix: '.json',
  });

  $translateProvider.preferredLanguage('fr');
}]);
