import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import { name as Navigation } from '../imports/components/navigation/navigation';

import { name as EquipmentsList } from '../imports/components/equipments/list';
import { name as EquipmentsView } from '../imports/components/equipments/view';
import { name as EquipmentsEdit } from '../imports/components/equipments/edit';
import { name as EquipmentsAnalyze } from '../imports/components/equipments/analyze';

import { name as EventsTypeEdit } from '../imports/components/eventsType/edit';

import { name as UsersList } from '../imports/components/users/list';
import { name as UsersConnection } from '../imports/components/users/connection';
import { name as UsersEdit } from '../imports/components/users/edit';

import { name as ExportsList } from '../imports/components/exports/list';
import { name as ExportsLog } from '../imports/components/exports/log';

import template from './app-body.html';

const name = 'home';

angular.module(name, [
  uiRouter,
  angularMeteor,
  Navigation,
  EquipmentsView,
  EquipmentsList,
  EquipmentsEdit,
  EquipmentsAnalyze,
  EventsTypeEdit,
  UsersList,
  UsersConnection,
  UsersEdit,
  ExportsList,
  ExportsLog,
])
.component(name, {
  template,
})
.config(['$stateProvider', '$locationProvider', '$urlRouterProvider', ($stateProvider, $locationProvider, $urlRouterProvider) => {
  'ngInject';

  const stateArray = [];

  // Home
  stateArray.push({
    name: 'home',
    url: '/home',
    template: '<p></p>',
  });


  // Users
  stateArray.push({
    name: 'users',
    abstract: true,
    url: '/users',
    template: '<ui-view></ui-view>',
  });

  // Users list
  stateArray.push({
    name: 'users.list',
    url: '/list',
    template: '<users-list></users-list>',
  });

  // User view
  stateArray.push({
    name: 'users.edit',
    url: '/edit/:userId',
    template: '<users-edit></users-edit>',
  });

  // User connection
  stateArray.push({
    name: 'users.connection',
    url: '/connection',
    template: '<users-connection></users-connection>',
  });


  // Equipments
  stateArray.push({
    name: 'equipments',
    abstract: true,
    url: '/equipments',
    template: '<ui-view></ui-view>',
  });

  // Equipments list
  stateArray.push({
    name: 'equipments.list',
    url: '/list',
    template: '<equipments-list></equipments-list>',
  });


  // Equipment view
  stateArray.push({
    name: 'equipments.view',
    url: '/view/:equipmentId/:po/:article',
    template: '<equipments-view></equipments-view>',
  });

  // Equipment edit
  stateArray.push({
    name: 'equipments.edit',
    url: '/edit/:equipmentId',
    template: '<equipments-edit></equipments-edit>',
  });

  // Equipment analyze
  stateArray.push({
    name: 'equipments.analyze',
    url: '/analyze',
    template: '<equipments-analyze></equipments-analyze>',
  });


  // Events type
  stateArray.push({
    name: 'eventsType',
    abstract: true,
    url: '/events-type',
    template: '<ui-view></ui-view>',
  });

  // Events type edit
  stateArray.push({
    name: 'eventsType.edit',
    url: '/edit',
    template: '<events-type-edit></events-type-edit>',
  });

  // Exports
  stateArray.push({
    name: 'exports',
    abstract: true,
    url: '/exports',
    template: '<ui-view></ui-view>',
  });

  // Exports list
  stateArray.push({
    name: 'exports.list',
    url: '/list',
    template: '<exports-list></exports-list>',
  });

  // Exports log
  stateArray.push({
    name: 'exports.log',
    url: '/log',
    template: '<exports-log></exports-log>',
  });


  // Inject all states
  _.each(stateArray, (value) => {
    $stateProvider.state(value);
  });


  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('home');
}]);
