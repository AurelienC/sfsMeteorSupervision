import '../imports/startup/server';
// import '../imports/startup/both';

// import modbus from 'jsmodbus';
// import Cron from 'cron';
// import { Meteor } from 'meteor/meteor';
// // import { ModbusPoolling } from '../server/modbus/modbus';
// import { Machines } from '../imports/api/machines';
//
//
//
//
//
// class ModbusPoolling {
//   constructor(machine) {
//     this.machine = machine;
//
//     this.client = modbus.client.tcp.complete({
//       host: this.machine.connection.ip,
//       port: this.machine.connection.port,
//       autoReconnect: true,
//       reconnectTimeout: 1000,
//       timeout: 5000,
//       unitId: this.machine.connection.modbusId,
//     }).connect();
//
//     this.client.on('connect', () => {
//       console.log(`[cron][${this.machine.name}] CONNECT`);
//     });
//
//     this.client.on('error', () => {
//       console.log(`[cron][${this.machine.name}] ERROR`);
//     });
//
//     this.client.on('error', () => {
//       console.log(`[cron][${this.machine.name}] CLOSE`);
//     });
//   }
//
//   poolling() {
//     this.client.readHoldingRegisters(this.machine.connection.offset, 38).then(() => {
//       console.log(`[cron][${this.machine.name}] readed`);
//
//     }, console.error);
//   }
//
//   stop() {
//     this.client.close();
//   }
// }
//
// Meteor.startup(() => {
//   // code to run on server at startup
//   const CronJob = Cron.CronJob;
//   const machines = Machines.find();
//   const machinesJobs = [];
//   const machinesMb = [];
//
//
//   function createJob(m) {
//     if (typeof machinesJobs[m._id] !== 'undefined') {
//       if (machinesJobs[m._id].cron.running) {
//         machinesJobs[m._id].cron.stop();
//         machinesMb[m._id].stop();
//       }
//     }
//
//     if (m.active) {
//       console.log(`[cron][${m.name}] creation with cron ${m.cron}`);
//       machinesMb[m._id] = new ModbusPoolling(m);
//       machinesJobs[m._id] = {
//         machine: m,
//         cron: new CronJob(m.cron, () => {
//           machinesMb[m._id].poolling();
//         }, null, true, 'Europe/Paris'),
//       };
//     }
//   }
//
//
//   machines.observe({
//     changed: (newMachine, oldMachine) => {
//       console.log('#### CHANGE');
//       console.log(newMachine);
//       if (newMachine.cron !== oldMachine.cron || newMachine.active !== oldMachine.active) {
//         createJob(newMachine);
//       }
//     },
//   });
//
//
//   machines.forEach((m) => {
//     createJob(m);
//   });
// });
